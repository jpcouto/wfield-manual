wfield
======

Tools for widefield analysis, manual registration and matching structures from the allen common reference framework.

![picture](images/retinotopy_example.png)



Installation
------------

do _python setup.py develop_ to install

Dependencies:

* skimage
* numpy
* opencv3

For the allen tools you will need the allensdk and the nrrd packages.


pip install allensdk
pip install pynrrd


Usage
-----

Check the examples in the notebooks directory.


To launch the GUI for matching widefield to the Allen Common Coordinate Framework:

**NOTE**: ``wfield-ccf-match --help`` for options.

wfield-ccf-match <RAW widefield tif> "average computed file or search pattern"

Keys:

* p - plot figures
* s - save parameters to json


Joao Couto
Feb, 2019
