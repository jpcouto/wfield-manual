import time
import sys
import numpy as np
import os
from glob import glob
from os.path import join as pjoin
from tifffile import TiffWriter as twriter
from tifffile import imread,TiffFile
import pandas as pd


class TiffSequence(object):
    def __init__(self,filenames):
        ''' 
        Class to load sequences of multipage tif files.
        '''
        if type(filenames) is str:
            filenames = np.sort(glob(pjoin(filenames,'*.tif')))
        assert type(filenames) in [list,np.ndarray], 'Pass a list of filenames.'
        self.filenames = filenames
        for f in filenames:
            assert os.path.exists(f), f + ' not found.'
        # Get an estimate by opening only the first and last files
        framesPerFile = []
        self.files = []
        for i,fn in enumerate(self.filenames):
            if i == 0 or i == len(self.filenames)-1:
                self.files.append(TiffFile(fn))
            else:
                self.files.append(None)                
            f = self.files[-1]
            if i == 0:
                dims = f.series[0].shape
                self.shape = dims
            elif i == len(self.filenames)-1:
                dims = f.series[0].shape
            framesPerFile.append(np.int64(dims[0]))
        self.framesPerFile = np.array(framesPerFile, dtype=np.int64)
        self.framesOffset = np.hstack([0,np.cumsum(self.framesPerFile[:-1])])
        self.nFrames = np.sum(framesPerFile)
        self.curfile = 0
        self.curstack = self.files[self.curfile].asarray()
        N,self.h,self.w = self.curstack.shape[:3]
        self.dtype = self.curstack.dtype
        self.shape = (self.nFrames,self.shape[1],self.shape[2])
    def getFrameIndex(self,frame):
        '''Computes the frame index from multipage tiff files.'''
        fileidx = np.where(self.framesOffset <= frame)[0][-1]
        return fileidx,frame - self.framesOffset[fileidx]
    def __getitem__(self,*args):
        index  = args[0]
        if not type(index) is int:
            Z, X, Y = index
            if type(Z) is slice:
                index = range(Z.start, Z.stop, Z.step)
            else:
                index = Z
        else:
            index = [index]
        img = np.empty((len(index),self.h,self.w),dtype = self.dtype)
        for i,ind in enumerate(index):
            img[i,:,:] = self.getFrame(ind)
        return np.squeeze(img)
    def getFrame(self,frame):
        ''' Returns a single frame from the stack '''
        fileidx,frameidx = self.getFrameIndex(frame)
        if not fileidx == self.curfile:
            if self.files[fileidx] is None:
                self.files[fileidx] = TiffFile(self.filenames[fileidx])
            self.curstack = self.files[fileidx].asarray()
            self.curfile = fileidx
        return self.curstack[frameidx,:,:]
    def __len__(self):
        return self.nFrames
    
    def todask(self):
        from dask import array as da
        from dask import delayed
        files = self.filenames
        delayed_imread = delayed(imread, pure=True)  # Lazy version of imread
        framedims = self.framesPerFile
        lazy_values = []
        for f,d in zip(files,framedims):
            lazy_values.append([delayed_imread(f),(d,self.shape[1],self.shape[2])])
            
        arrays = [da.from_delayed(v0,        
                                  dtype=self.dtype, 
                                  shape=v1)
                  for v0,v1 in lazy_values]
        return da.concatenate(arrays, axis=0)

