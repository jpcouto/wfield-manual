import numpy as np
from skimage.transform import rescale, resize, rotate
from skimage.filters import gaussian as im_gaussian
from skimage.measure import CircleModel
import scipy.ndimage as ndimage
from copy import deepcopy
from natsort import natsorted as sortstr
from .imutils import *

def fit_circle(points,npoints = 1000):
    '''
    Fit a circle to a set of points.
    Returns the xy points on the circle and the circle parameters.
    '''
    from skimage.measure import CircleModel
    cmod = CircleModel()
    cmod.estimate(np.stack(points))
    return cmod.predict_xy(np.linspace(0,2*np.pi,npoints)), cmod.params

def adjust_allen_areas(refregions,
                       translation = np.array([0,0]),
                       rotation=0,
                       scale = 1,
                       reference_structure = 'VISp',**kwargs):
    """scale*rotate_origin(r['cm']-refpoint,rotation)
    translate, rotate and scale reference structures.scale*rotate_origin(r['cm']-refpoint,rotation)
    
    """
    refstruct = list(filter(lambda x: x['name']==reference_structure,refregions))[0]
    refpoint = refstruct['cm']
    res = [dict(**r) for r in deepcopy(refregions)]
    for r in res:
        r['c'] = scale*rotate_points(r['c'],rotation,origin = refpoint) + translation-refpoint
        r['cm'] = scale*rotate_points(r['cm'],rotation,origin = refpoint) + translation - refpoint
    return res

def adjust_allen_areas_inverse(refregions,original_refregions,
                              translation = np.array([0,0]),
                              rotation=0,
                              scale = 1,
                              reference_structure = 'VISp',
                              **kwargs):
    """
        invert a translate, rotate and scale from window to allen coords.
    
    """
    refstruct = list(filter(lambda x: x['name']==reference_structure,
                            original_refregions))[0]
    refpoint = refstruct['cm'] 
    res = [dict(**r) for r in deepcopy(refregions)]
    for r in res:
        r['c'] = rotate_points((r['c'] -(translation-refpoint))/scale,-1*rotation,refpoint)  
        r['cm'] = rotate_points((r['cm'] -(translation - refpoint))/scale,-1*rotation,refpoint)
    return res


def points_to_allen_areas(points,original_refregions,
                          translation = np.array([0,0]),
                          rotation=0,
                          scale = 1,
                          reference_structure = 'VISp',
                          **kwargs):
    """
        invert a translate, rotate and scale from window to allen coords.
    
    """
    refstruct = list(filter(lambda x: x['name']==reference_structure,
                            original_refregions))[0]
    refpoint = refstruct['cm'] 
    return rotate_points((points - translation + refpoint)/scale,-1*rotation ,refpoint)  


def adjust_to_reference(fitim,refh,refw,
                        rotation = 0,
                        scale = 0.5,
                        transpose = True,
                        ratio = 1.,
                        origin = None,**kwargs):
    '''
    Adjust an image to fit a reference.
    translate,rotate,set aspect ratio,scale

    adjust_to_reference(fitim,refh,refw,
                        rotation = 0,
                        scale = 0.5,
                        transpose = True,
                        ratio = 1.,
                        origin = None)
    Returns an adjusted image that can be overlayed on the reference.
    '''
    if origin is None:
        # center on the other image if not defined
        origin = [refh/2,refw/2]
    out = np.zeros([refh,refw])
    out[:] = np.nan
    if transpose:
        fitim  = np.transpose(fitim,[1,0])
    h,w = fitim.shape
    # scale image
    tmp = resize(fitim,[int(h*scale),
                        int(w*scale)],
                 anti_aliasing = True,
                 mode = 'reflect')
    th,tw = tmp.shape
    # set the aspect ratio
    tmp = resize(tmp,
                 [int(th),int(tw*ratio)],
                 anti_aliasing = True,
                 mode = 'reflect')
    th1,tw1 = tmp.shape
    tmp = rotate(tmp,rotation,resize = True)
    tmp[tmp==0] = np.nan
    th,tw = tmp.shape
    origin =  np.array(origin,dtype = int)
    # need to compensate for the rotation of the center
    oh = int(np.floor((th/2)))
    ow = int(np.floor((tw/2)))
    a = origin[0]-oh
    b = origin[0]+th-oh
    c = origin[1]-ow
    d = origin[1]+tw-ow
    # Deal with out of bounds
    if a<0:
        tmp = tmp[abs(a):,:]
    if c<0:
        tmp = tmp[:,abs(c):]
    if b > refh:
        tmp = tmp[:refh-b,:]
    if d > refw:
        tmp = tmp[:,:refw-d]
    a = int(np.clip(a,0,refh))
    b = int(np.clip(b,0,refh))
    c = int(np.clip(c,0,refw))
    d = int(np.clip(d,0,refw))
    out[a:b,c:d] = tmp[:,:]
    return out

def adjust_points_to_reference(xy,dims,
                               rotation = 0,
                               scale = 0.5,
                               transpose = True,
                               ratio = 1.,
                               origin = None,
                               **kwargs):
    ''' 
    To shift ROI contours 
    
    Example:

    res = adjust_points_to_reference(fitim,**fitpars[0])
    plt.imshow(res**0.5,cmap = 'gray_r')
    for (c1,c2) in cont:
        cc = adjust_points_to_reference(c1,dims = fitim.shape,**fitpars[0])
        plt.plot(cc[:,0],cc[:,1],'r')
    c2 = np.stack([c[1] for c in cont])
    cc = adjust_points_to_reference(c2,dims = fitim.shape,**fitpars[0])
    plt.plot(cc[:,0],cc[:,1],'ko')

    '''
    xy = xy.copy()
    if transpose:
        xy = np.fliplr(xy)
    #xy[:,0] = dims[1] - xy[:,0]
    #dims1 = dims[1]*scale
    xy = xy*scale
    xy[:,0] = xy[:,0]*ratio

    rorigin = np.array([(dims[1]*scale)*ratio/2.,dims[0]*scale/2])
    xy = rotate_points(xy,rotation,rorigin)
    #xy = np.flipud(dims) - xy
    xy += np.flipud(origin)-rorigin #+ np.flipud(get_translation_from_rotate_resize(rorigin * 2,rotation)) 
    return xy

def points_to_extent(points,dims,extent,origin_top = False):
    ''' 
    Place points in extent
    
    Example:
    
    cc = points_to_extent(
         adjust_points_to_reference(c2,dims = fitim.shape,**fitpars[0]),
                      refwin.shape, extent)
    '''
    # normalize to dims
    res = np.array(points).copy()
    res /= np.array(np.flipud(dims))
    if origin_top:
        # flip because the origin starts on top
        res[:,1] = 1 - res[:,1]
    extentorigin = np.array([extent[2],extent[1]])
    extentdiff = np.abs(np.array([np.diff([extent[2],extent[3]]),
                                  np.diff([extent[0],extent[1]])])).flatten()
    res *= extentdiff
    res -= extentorigin
    return res

def estimate_sbx_lost_lines(sbximage, trim = True):
    ''' Estimates the lines lost by scanbox and either interpolates or trims the lost lines. 
    Default is trimming.

    Usage:
        sbximage = estimate_sbx_lost_lines(sbximage)
    '''
    cidx = np.where(sbximage.std(axis = 0)>5000)[0]
    if len(cidx):
        cidx = cidx[-1]+1
        if not trim:
            sbximage[1::2,:cidx] = sbximage[0::2,:cidx]
        else:
            sbximage = sbximage[:,cidx:]
    return sbximage


def make_circle_mask(circlepar,oriim):
    '''
    Return a mask from circle parameters 
    Usage:

        mask = make_circle_mask(resmanual['circlepar'],oriim)
        oriim[mask == 0] = np.nan

    '''
    t = np.linspace(0, 2 * np.pi, 2000)
    xy = CircleModel().predict_xy(t, params=circlepar)#*resmanual['resolution']
    r_mask = np.zeros_like(oriim, dtype='bool')
    h,w = r_mask.shape
    xy[:,0] = np.clip(xy[:,0],0,h-1)
    xy[:,1] = np.clip(xy[:,1],0,w-1)
    r_mask[np.round(xy[:, 0]).astype('int'), np.round(xy[:, 1]).astype('int')] = 1
    return ndimage.binary_fill_holes(r_mask)

def runpar(f,X,nprocesses = None,**kwargs):
    ''' 
    res = runpar(function,          # function to execute
                 data,              # data to be passed to the function
                 nprocesses = None, # defaults to the number of cores on the machine
                 **kwargs)          # additional arguments passed to the function (dictionary)

    '''
    from multiprocessing import Pool,cpu_count
    from functools import partial
    if nprocesses is None:
        nprocesses = cpu_count()
    pool = Pool(processes=nprocesses)
    res = pool.map(partial(f,**kwargs),X)
    pool.close()
    pool.join()
    return res

import json
class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(JsonEncoder, self).default(obj)
