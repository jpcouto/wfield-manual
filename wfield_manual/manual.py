import pylab as plt
import numpy as np
from .utils import *
from .plotutils import *

def manual_sample_window_scale(im, windowsize = 5):
    '''
    Get the resolution from sampling the edges of an imaging window.
    Example:
        oriim = imread('20190124_run001_00000000.tif').mean(axis = 0)
        resmanual = manual_sample_window_scale(oriim)
    Output:
    resmanual = {'circlexy': xy points for plotting
                 'points': manually selected points
                 'circlepar': circle parameters
                 'resolution': resolution (mm/pixels)
    '''
    res = dict(circlexy = [],
               points = [],
               circlepar = [])
    fig = plt.figure()
    ax = fig.add_axes([.1,.1,.8,.8])
    ax.imshow(im,cmap='gray')
    points = []
    p = ax.plot([],[],'go-')
    p.extend(ax.plot([],[],'r-'))
    def moved_and_pressed(event):
        if not event.button is None:
            points = res['points']
            if event.button==1:
                xy = [int(np.floor(event.ydata)),int(np.floor(event.xdata))]
                points.append(xy)
            p[0].set_xdata([p[1] for p in points])
            p[0].set_ydata([p[0] for p in points])
            xy,par = fit_circle(np.stack(points))
            p[1].set_xdata(xy[:,1])
            p[1].set_ydata(xy[:,0])
            res['circlexy'] = xy
            res['circlepar'] = par
            res['points'] = points
            res['resolution'] = windowsize/round(2*res['circlepar'][2])
        fig.canvas.draw_idle()
        fig.canvas.flush_events()
    fig.canvas.mpl_connect('button_press_event', moved_and_pressed)
    return res

def manual_overlay_allen_areas(refregions,refpar):
    '''
    Manually overlay reference structure map
    Keys:
        - scale [g,f]
        - rotation [b,v]
        - aspect ratio [y,t]
        - origin [up,down,left,right]
        - transpose [h]
        - alpha [a,z]
    Returns a dictionary "fitpar" that can be passed to

    Usage:
        fitpar = manual_overlay_image(refim,fitim,fitpar)
        fittedimage = adjust_to_reference(fitim,**fitpar)
    '''
    fig = plt.gcf()
    res = adjust_allen_areas(refregions,**refpar)
    p,t = plot_allen_areas(res)
    xy = []
    def moved_and_pressed(event):
        if fig.canvas.manager.toolbar._active is None:
            if not event.button is None:
                if event.button==1:
                    xy = [event.ydata,event.xdata]
                    refpar['translation'] = np.array(xy)
                    res = adjust_allen_areas(refregions,
                                             **refpar)
                plot_allen_areas(res,refresh = [p,t])

                fig.canvas.draw_idle()
                fig.canvas.flush_events()

    def keypress(event):
        key = event.key
        #t.set_text(key)
        if key in ['+','-',
                   'up','down',
                   '2','8','4','6',
                   'left','right',
                   'f','d','c','v']:
            if key == 'f':
                refpar['scale'] *= 1.01
            elif key == 'g':
                refpar['scale'] *= 0.99
            elif key in ['down','2']:
                refpar['translation'][0] += 0.1
            elif key in ['up','8']:
                refpar['translation'][1] -= 0.1
            elif key in ['right','6']:
                refpar['translation'][0] -= 0.1
            elif key in ['left','4']:
                refpar['translation'][0] += 0.1
            elif key in ['c']:
                refpar['rotation'] += 1
            elif key in ['v']:
                refpar['rotation'] -= 1
            #elif key in ['f']:
            #    flipv = not flipv
            res = adjust_allen_areas(refregions,
                                     **refpar)
            plot_allen_areas(res,refresh = [p,t])
        fig.canvas.draw_idle()
        fig.canvas.flush_events()            

    fig.canvas.mpl_connect('key_press_event', keypress)            
    fig.canvas.mpl_connect('motion_notify_event', moved_and_pressed)
    fig.show()
    return refpar

def manual_overlay_image(refim,fitim,fitpar = None):
    '''
    Manually overlay an image to a reference
    
    Keys:
        - scale [g,f]
        - rotation [b,v]
        - aspect ratio [y,t]
        - origin [up,down,left,right]
        - transpose [h]
        - alpha [a,z]
    Returns a dictionary "fitpar" that can be passed to

    Usage:
        fitpar = manual_overlay_image(refim,fitim,fitpar)
        fittedimage = adjust_to_reference(fitim,**fitpar)
    '''
    h,w = refim.shape
    if fitpar is None:
        fitpar = dict(refh = h,refw = w, rotation = 0, scale = 0.2, ratio = 1,origin = [100,100])
    res = adjust_to_reference(fitim,**fitpar)
    
    fig = plt.gcf()
    plt.imshow(refim,cmap = 'gray')
    disp = plt.imshow(res,alpha = 0.5,cmap = 'hot')
#     t = plt.text(0,0,'Nothing')
    def moved_and_pressed(event):
        if fig.canvas.manager.toolbar._active is None:
            if not event.button is None:
                if event.button==1:
                    xy = [event.ydata,event.xdata]
                    fitpar['origin'] = xy
                    res = adjust_to_reference(fitim,
                                             **fitpar)
                    disp.set_data(res)

                fig.canvas.draw_idle()
                fig.canvas.flush_events()

    def keypress(event):
        key = event.key
        #t.set_text(key)
        if key == 'f':
            fitpar['scale'] *= 1.01
        elif key == 'g':
            fitpar['scale'] *= 0.99
        elif key in ['y']:
            fitpar['ratio'] *= 1.05
        elif key in ['t']:
            fitpar['ratio'] *= 0.95
        elif key in ['down','2']:
            fitpar['translation'][1] -= 0.1
        elif key in ['up','8']:
            fitpar['translation'][1] += 0.1
        elif key in ['right','6']:
            fitpar['translation'][0] -= 0.1
        elif key in ['left','4']:
            fitpar['translation'][0] += 0.1
        elif key in ['b']:
            fitpar['rotation'] += 2
        elif key in ['v']:
            fitpar['rotation'] -= 2
        elif key in ['h']:
            fitpar['transpose'] = not fitpar['transpose']
        elif key in ['a']:
            disp.set_alpha(disp.get_alpha()*0.8)
        elif key in ['z']:
            disp.set_alpha(disp.get_alpha()*1.2)
        res = adjust_to_reference(fitim,**fitpar)
        disp.set_data(res)
        fig.canvas.draw_idle()
        fig.canvas.flush_events()            

    fig.canvas.mpl_connect('key_press_event', keypress)            
    fig.canvas.mpl_connect('motion_notify_event', moved_and_pressed)
    fig.show()
    return fitpar
