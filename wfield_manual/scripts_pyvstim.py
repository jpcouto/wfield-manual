from labcams.utils import findVStimLog,cameraTimesFromVStimLog
from pyvstim import (parseVStimLog,
                     getStimuliTimesFromLog,
                     treadmillBehaviorFromRelativePosition)
from labcams.io import parseCamLog,TiffStack
from glob import glob
import numpy as np
import os
import sys
from os.path import join as pjoin
from .rawutils import *
from scipy.interpolate import interp1d

def get_stimuli_times_from_log(vstimlog,log):
    stimtimes,stimpars,stimopt = getStimuliTimesFromLog(vstimlog,log)
    
def find_nloops_from_prot(stimpar):
    '''
    Finds the number of loops by iterating over the protocol description.

    This should probably be in pyvstim.
    '''
    nloops = []
    for iStim in range(len(stimpar)):
        for p in stimpar.iloc[iStim]:
            nloop = 1
            if isinstance(p, str):
                if 'loop' in p:
                    nloop = int(p.strip(')').split(',')[-1])
                    break
        nloops.append(nloop)
    return nloops


def combine_loops(trigavg,stimpar,wpre=0):
    nloops = find_nloops_from_prot(stimpar)
    for i,stimavg in enumerate(trigavg):
        if nloops[i] > 1:
            print('There are {0} loops for stim {1}.'.format(nloops[i],i))
            idx = np.where(stimavg[:,0,0] >
                           np.min(stimavg[:,0,0]))[0]
            # skip if no stim indicator...
            if not len(idx)>2:
                idx = np.arange(len(stimavg))
            looplen = int(np.ceil(len(idx)/nloops[i]))
            single_loop = np.zeros([looplen,
                                    stimavg.shape[1],
                                    stimavg.shape[2]],
                                   dtype = np.float32)
            for nloop in range(nloops[i]):
                single_loop += stimavg[
                    idx[0] + nloop*looplen : idx[0] +
                    (nloop+1)*looplen,:,:]
            single_loop /= float(nloops[i])
            single_loop[:,:10,:10] = single_loop[0,11,11] 
            trigavg[i] = single_loop
    return trigavg


def get_camera_time(camlogfile,plog,camidx=3,nframes = None):
    if not len(camlogfile):
        print('\n\n Camera logfile not found in: {0}'.format(camlogfile))
        camlogfile = None
        camtime = interp1d(
            plog['cam1'.format(camidx)]['value'].iloc[[0,-1]],
            plog['cam1'.format(camidx)]['duinotime'].iloc[[0,-1]]/1000.,
            fill_value="extrapolate")(np.arange(nframes))
    else:
        camlogfile = camlogfile[0]
        camlog = parseCamLog(camlogfile)[0]
        camlog = cameraTimesFromVStimLog(camlog,plog,camidx = camidx)
        camtime = np.array(camlog['duinotime']/1000.)
    
    return camtime

def get_logs_and_cams(foldername,camidx = 3):
    '''
    get logs and open a tiff stack (expfolder is the datafolder path)
    
    Example:
        fname = str('/quadraraid/data/1photon/raw/190124_JC092_1P_JC/run00_closedLoopActuator')
        expname,plog,logfile,camtime,camdata = get_logs_and_cams(fname)
    '''
    expname = foldername.split(os.path.sep)[-2:]
    if not camidx is None:
        camlogext = '.camlog'
        camlogfile = glob(pjoin(foldername,'*'+camlogext))
        
        if not len(camlogfile):
            display('\n\n Camera logfile not found in: {0}'.format(foldername))
            camlogfile = None
        else:
            camlogfile = camlogfile[0]
            camlog = parseCamLog(camlogfile)[0]
    logfile = findVStimLog(expname)
    if not len(logfile):
        print('Could not find pyvstim log file.')
        sys.exit()
    plog,pcomms = parseVStimLog(logfile)
    camdata = TiffStack(foldername)
    if not camidx is None and not camlogfile is None:
        camlog = cameraTimesFromVStimLog(camlog,plog,camidx = camidx)
        camtime = np.array(camlog['duinotime']/1000.)
    else:
        print('Using camidx 1 to aproximate the frame times.')
        camtime = np.linspace(plog['cam1'].duinotime.iloc[0]/1000.,
                              plog['cam1'].duinotime.iloc[-1]/1000.,len(camdata))
    return expname,logfile,plog,camtime,camdata

def get_lap_triggered(expname,plog,camtime,camdata,
                      laplength = 150.,
                      lapbin = 1.,
                      lapidx = None,
                      baseline = None,
                      global_baseline=False,
                      do_df_f = True,
                      save = False,
                      progress = None):
    '''
    If the baseline is not provided, it is computed from the 
average of 5000 frames during locomotion.
    '''
    (behaviortime,position,
     displacement,velocity,
     laptimes) = treadmillBehaviorFromRelativePosition(
         np.array(plog['position'].duinotime,dtype=np.float32)/1000.,
         np.array(plog['position'].value,dtype=np.float32))
    
    from scipy.interpolate import interp1d
    npos = interp1d(behaviortime,position,
                    fill_value = "extrapolate",
                    bounds_error=False)(camtime)*laplength
    nvel = interp1d(behaviortime,velocity,
                    fill_value = 0,
                    bounds_error=False)(camtime)
    laps = np.vstack([laptimes[:-1],laptimes[1:]]).T
    if lapidx is None:
        lapidx = [0,len(laps)-1]
    laps = laps[lapidx[0]:lapidx[1]]
    if global_baseline and baseline is None:
        runframes = np.where(nvel*laplength > 1.)[0]
        if len(runframes) > 5000:
            runframes = runframes[:5000]
            print('Trimmed frames for baseline')
        tmp = camdata[runframes,:,:]
        baseline = np.nanmean(tmp,axis =0).astype(np.float32)
        print('Computed baseline.')
    print('Computing lap maps for {0} laps.'.format(len(laps)))
    lapFrames = bin_frames_to_laps(laps,camtime,
                                   npos,
                                   camdata,
                                   velocity = nvel*laplength,
                                   lapspace = np.arange(0,laplength,lapbin),
                                   baseline = baseline,
                                   global_baseline = global_baseline,
                                   do_df_f = do_df_f,
                                   baseline_method = lambda x: np.nanmean(
                                       x,axis = 0),progress = progress)
    if save:
        from labcams import getPreferences
        parameters = getPreferences(create = False)
        fname = pjoin(parameters['datapaths']['dataserverpaths'][0],
                      parameters['datapaths']['analysispaths'],
                      expname[0],expname[1],'stimaverages_cam{0}'.format(
                          camidx),
                      'lapFrames.tif')
        if not os.path.isdir(os.path.dirname(fname)):
            os.makedirs(os.path.dirname(fname))
            from tifffile import imsave
            imsave(fname,lapFrames)
            print('Saved {0}'.format(fname))
    return lapFrames,baseline

def get_stim_triggered(expname,logfile,plog,camtime,camdata,
                       tpre = None,
                       do_df_f = True,
                       global_baseline = False,
                       save = False):
    '''
    stimavgs = get_stim_triggered(expname,logfile,plog,camtime,camdata,
                       do_df_f = True,
                       global_baseline = False,
                       save = False)
    '''
    from pyvstim import parseProtocolFile
    protopts,prot = parseProtocolFile(logfile.replace('.log','.prot'))
    (stimtimes,stimpars,stimoptions) = getStimuliTimesFromLog(
        logfile,plog)
    if tpre is None:
        tpre = 0
        if 'BlankDuration' in protopts.keys():
            tpre = float(protopts['BlankDuration'])/2.
    stimavgs = triggered_average(
        camdata,camtime,
        stimtimes,
        tpre = tpre,
        global_baseline = global_baseline,
        do_df_f = do_df_f)
    # remove loops if there
    for iStim in range(len(stimavgs)):
        nloops = 0
        for p in prot.iloc[iStim]:
            if isinstance(p, str):
                if 'loop' in p:
                    nloops = int(p.strip(')').split(',')[-1])
        if nloops > 0:
            display('Handling loops for stim {0}.'.format(iStim))
            idx = np.where(stimavgs[iStim][:,0,0] >
                           np.min(stimavgs[iStim][:,0,0]))[0]
            looplen = int(np.ceil(np.shape(stimavgs[iStim][idx])
                                  [0]/nloops))
            single_loop = np.zeros([looplen,
                                    stimavgs[iStim].shape[1],
                                    stimavgs[iStim].shape[2]],
                                   dtype = np.float32)
            for nloop in range(nloops):
                single_loop += stimavgs[iStim][
                    idx[0] + nloop*looplen : idx[0] +
                    (nloop+1)*looplen,:,:]
            single_loop /= float(nloops)
            stimavgs[iStim] = single_loop
    if save:
        from labcams import getPreferences
        parameters = getPreferences(create = False)
        for iStim,savg in enumerate(stimavgs):
            fname = pjoin(parameters['datapaths']['dataserverpaths'][0],
                          parameters['datapaths']['analysispaths'],
                          expname[0],expname[1],'stimaverages_cam{0}'.format(camidx),
                          'stim{0}.tif'.format(iStim))
            if not os.path.isdir(os.path.dirname(fname)):
                os.makedirs(os.path.dirname(fname))
            from tifffile import imsave
            print(fname)
            imsave(fname,savg)
    return stimavgs
