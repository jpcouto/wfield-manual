
import numpy as np
import cv2
from skimage import img_as_float,exposure
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.filters import median_filter
from scipy.interpolate import interp1d
from scipy.ndimage import morphology
import math

def medfilt2d(X, size = int(7)):
    '''
    2d median filter a movie of images in parallel
    '''
    from .utils import runpar
    return np.stack(runpar(median_filter,X,size = size))

def gaussfilt2d(X,size = int(7)):
    '''
    2d gaussian filter a movie of images in parallel
    '''
    from .utils import runpar
    return np.stack(runpar(gaussian_filter,X,sigma = size))

def im_adapt_hist(im,clip_limit = 0.05):
    ''' Adaptative histogram from image 
    (datatype sensitive,outputs as float between -1 and 1)
    Usage:
        eqim = im_adapt_hist(im,0.1)
    '''
    ii = img_as_float(im)
    return exposure.equalize_adapthist(ii, clip_limit=clip_limit)

def hsv_colorbar_image():
    '''
    hsv_colorbar_image()
    
    
    Dumb way to make a colorbar:for an hsv image
    cbarim = hsv_colorbar_image()
    plt.imshow(cbarim,aspect= 'auto',extent = [0,1,-40,40])
    
    '''
    hsvimg = np.ones([1,255,3])*255
    hsvimg[:,:,0] = np.linspace(0,255,255)
    hsvimg = hsvimg.transpose([1,0,2])
    return cv2.cvtColor(hsvimg.astype(np.uint8), cv2.COLOR_HSV2RGB_FULL)




def rotate_origin(xy, degrees):
    """
    Rotate a 2d point around the origin (0, 0).
    usage:
        ncoords = rotate_origin(coords, degrees = 30)
    """
    return rotate_points(xy,degrees)

def rotate_points(xy, degrees,origin = (0,0)):
    """
    Rotate a 2d point around the a point.
    usage:
        ncoords = rotate_origin(coords, degrees = 30)
    """
    radians = np.deg2rad(degrees)
    x, y = xy.T
    xo, yo = origin
    x_ = (x - xo)
    y_ = (y - yo)

    xx = xo + x_ * math.cos(radians) + y_ * math.sin(radians)
    yy = yo + -x_ * math.sin(radians) + y_ * math.cos(radians)
    return np.stack([xx,yy]).T

def find_point_region(point,refregions):
    '''
    Example:
    refregion = load_refregions()
    point = [6.5,3.0]
    ireg,region_name,distance = find_point_region(point,refregions)
    
    '''
    iregion = -1
    region_name = ''
    dist = -1
    for ir, region in enumerate(refregions):
        c = region['c']
        d = cv2.pointPolygonTest(c.astype(np.float32),
                                    tuple(point),True)
        if d > 0:
            iregion = ir
            region_name = region['name']
            dist = d
            break
    return iregion,region_name,dist

def bin_contour_on_extent(cont,shape,extent,n_up_samples = 1000):
    '''
    H = bin_contour_on_extent(cont,shape,extent,n_up_samples = 1000)
Usage:
    C = refregions[0]['c']
    H = bin_contour_on_extent(C,oriim.shape,extent)
    plt.imshow(H,extent=extent,cmap = 'gray')  # Let each row list bins with common y range.
    plt.plot(C[:,1],C[:,0],alpha = 0.3,color='y')

'''
    x = np.linspace(extent[0], extent[1], shape[0]+1)
    y = np.linspace(extent[2], extent[3], shape[1]+1)
    C = cont.copy()
    C = np.vstack([C,C[0,:]])
    if n_up_samples > cont.shape[0]:
        C = np.zeros((n_up_samples,2))
        C[:,0] = interp1d(np.linspace(0,1,cont.shape[0]),cont[:,0])(np.linspace(0,1,n_up_samples))
        C[:,1] = interp1d(np.linspace(0,1,cont.shape[0]),cont[:,1])(np.linspace(0,1,n_up_samples))
    H, xedges, yedges = np.histogram2d(C[:,0], C[:,1], bins=(np.sort(x), np.sort(y)))
    H = H>0
    return H.astype(bool)

def mask_contour_on_extent(cont,shape,extent,n_up_samples = 10000):
    '''
        H = mask_contour_on_extent(cont,shape,extent,n_up_samples = 1000)
    Usage:
        C = refregions[0]['c']
        H = bin_contour_on_extent(C,oriim.shape,extent)
        plt.imshow(H,extent=extent,cmap = 'gray')  # Let each row list bins with common y range.
        plt.plot(C[:,1],C[:,0],alpha = 0.3,color='y')
    '''

    H = bin_contour_on_extent(cont = cont, 
                              shape = shape,
                              extent = extent,
                              n_up_samples = n_up_samples)    
    # fix border cases
    if np.sum(H[0,:]):
        H[0,:] = np.uint8(1)
    if np.sum(H[-1,:]):
        H[-1,:] = np.uint8(1)
    if np.sum(H[:,0]):
        H[:,0] = np.uint8(1)
    if np.sum(H[:,-1]):
        H[:,-1] = np.uint8(1)
    H = morphology.binary_dilation(H)
    H = morphology.binary_fill_holes(H)
    H = morphology.binary_erosion(H)
    H[0,:] = 0
    H[-1,:] = 0
    H[:,0] = 0
    H[:,-1] = 0
    return H.astype(bool)

def mask_to_3d(mask,shape,include_mask = None):
    '''
    3dmask = mask_to_3d(mask,shape,include_mask = None)
    
    Broadcasts a mask to a 3d array.

    Usage:
s1mask = mask_contour_on_extent(nrefregions[2]['c'],oriim.shape,extent=extent)[:,::-1]
v1mask = mask_contour_on_extent(nrefregions[6]['c'],oriim.shape,extent=extent)[:,::-1]
rlmask = mask_contour_on_extent(nrefregions[10]['c'],oriim.shape,extent=extent)[:,::-1]
s1moviemask = mask_to_3d([v1mask,s1mask,rlmask],tmp.shape,include_mask = winmask)
'''
    if type(mask) is list:
        mask = np.any(np.stack([m.astype(bool) for m in mask]),axis = 0)
    nmask = mask.copy().astype(mask.dtype)
    if not include_mask is None:
        nmask = np.all([nmask,include_mask],axis = 0)
    return np.broadcast_to(nmask,shape)

def get_signals_from_mask(dat, mask,include_mask = None):
    '''
    sigs = get_signals_from_mask(dat, mask,include_mask = None)

    Extract signals from a 2d or 3d array.
    
    Usage:
s1res = get_signals_from_mask(stimavgsf[0],s1mask,include_mask = winmask)
'''
    if type(mask) is list:
        mask = np.any(np.stack([m.astype(bool) for m in mask]),axis = 0)
    if not len(mask.shape) == len(dat.shape):
        mask = mask_to_3d(mask,dat.shape,include_mask = include_mask)
    res = dat[mask]
    if len(dat.shape) > 2:
        res = res.reshape((dat.shape[0],int(res.shape[0]/dat.shape[0])))
    return res
