import pylab as plt
import numpy as np
import pylab as plt

def imshow_window(im ,pars = dict(resolution=0.02),plot = True,**kwargs):
    ''' 
    Plots a window centered on the origin and in mm (set pars['resolution'])
    '''
    h,w = im.shape
    ah = pars['resolution']*(np.arange(h) - h/2)
    aw = pars['resolution']*(np.arange(w) - w/2)
    extent = [aw[0],aw[-1],ah[-1],ah[0]]
    if plot:
        plt.imshow(im,extent = extent,**kwargs)
    return extent


def hsl_cmap(ncolors = 50):
    '''Circular perceptually uniform colormap'''
    
    from matplotlib.colors import ListedColormap
    import seaborn as sns
    return ListedColormap(sns.color_palette("hls", ncolors))


def plot_allen_areas(refregions,
                     reference_structure = 'VISp',
                     plotnames = True,
                     refresh = None,
                     **kwargs):
    '''
    Plot or updates the reference structures defined by contours in reference to a reference structure.
    '''
    
    refpoint = [0,0]
    if refresh is None:
        p = []
        t = []
    else:
        p,t = refresh
    for i,r in enumerate(refregions):
        if not refresh is None:
            p[i].set_xdata(r['c'][:,1]-refpoint[1])
            p[i].set_ydata(r['c'][:,0]-refpoint[0])
        else:
            p.extend(plt.plot(r['c'][:,1]-refpoint[1],r['c'][:,0]-refpoint[0],**kwargs))
        if plotnames:
            if not refresh is None:
                t[i].set_position([r['cm'][1]-refpoint[1],r['cm'][0]-refpoint[0]])
            else:
                t.append(plt.text(r['cm'][1]-refpoint[1],
                                  r['cm'][0]-refpoint[0],
                                  r['name'],va = 'top',
                                  ha = 'center',
                                  fontsize = 8,
                color = 'k'))
    return p,t

def nb_play_movie(data,interval=30,**kwargs):
    ''' 
    Play a movie from the notebook
    '''
    from ipywidgets import Play,jslink,HBox,IntSlider
    from IPython.display import display

    im = plt.imshow(data[0],**kwargs)
    slider = IntSlider(0,min = 0,max = len(data)-1,step = 1,description='Frame')
    play = Play(interval=interval,
                value=0,
                min=0,
                max=len(data)-1,
                step=1,
                description="Press play",
                disabled=False)
    jslink((play, 'value'), (slider, 'value'))
    display(HBox([play, slider]))
    
    def updateImage(change):
        im.set_data(data[change['new']])
    slider.observe(updateImage, names='value')
    return dict(fig = plt.gcf(),
                ax=plt.gca(),
                im= im,
                update = updateImage)

def nb_save_movie(data,filename,interval = 100,dpi = 90,**kwargs):
    '''
    Replace nb_play_movie with this to save to a file.

    Example:

    nb_save_movie(tmp[:,:,::-1],
                  filename = '~/Desktop/example.avi',
                  clim = [.06,.2],
                  extent = extent,
                  cmap = 'hot', 
                  alpha = 0.5);
    '''
    from tqdm import tqdm
    from matplotlib.animation import FuncAnimation
    def animate(frame):
        global pbar
        pbar.update(1)
        im.set_data(data[frame])
        
        return im,
    fig = plt.gcf()
    im = plt.imshow(data[0],**kwargs)
    animation = FuncAnimation(
        fig,
        animate,
        np.arange(len(data)),
        fargs=[],
        interval=interval)
    global pbar
    pbar = tqdm(desc = 'Saving movie ',total=len(data))
    animation.save(filename, dpi=dpi)
    pbar.close()
    plt.show()
    print('Saved to {0}'.format(filename))
    
def set_cursor_info_image_coords():
    '''
    Adapted from https://stackoverflow.com/questions/27704490/interactive-pixel-information-of-an-image-in-python
    '''
    ax = plt.gca()
    def format_coord(x, y):
        imgs = ax.get_images()
        if len(imgs) > 0:
            for img in imgs:
                try:
                    array = img.get_array()
                    extent = img.get_extent()
                    x_space = np.linspace(extent[0], extent[1], array.shape[1])
                    y_space = np.linspace(extent[3], extent[2], array.shape[0])
                    x_idx= (np.abs(x_space - x)).argmin()
                    y_idx= (np.abs(y_space - y)).argmin()
                    z = array[y_idx, x_idx]
                    return 'x={0}({3}), y={1}({4}), z={2}'.format(x_idx, y_idx, z, x, y)
                except (TypeError, ValueError):
                    pass
            return 'x={0}, y={1}, z={2}'.format(x, y, 0)
        return 'x={0}, y={1}'.format(x, y)
    # end format_coord
    ax.format_coord = format_coord


