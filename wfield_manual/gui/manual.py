from .utils import *

__all__ = ['ManualFitCircle','ManualImageOverlay']


class ManualFitCircle(QWidget):
    def __init__(self,oriim = None,
                 resmanual=dict(circlepar = [222,111,291],resolution = 0.00857,windowsize = 5),
                 parent = None):
        super(ManualFitCircle,self).__init__()
        self.parent = parent
        self.resmanual = resmanual
        self.oriim = oriim
        self.eqori = True
        self.init_ui()
        
    def init_ui(self):
        layout = QFormLayout()
        self.setLayout(layout)

        self.win = pg.GraphicsLayoutWidget()
        self.p1 = self.win.addPlot()
        self.p1.getViewBox().invertY(True)
        self.p1.getViewBox().setAspectLocked(True)
        self.p1.getAxis('bottom').setPen('k') 
        self.p1.getAxis('left').setPen('k') 

        self.img = pg.ImageItem()
        self.p1.addItem(self.img)
        layout.addRow(self.win)
        
        self.img.setImage(self.oriim[:,::-1])
        self.circle = pg.CircleROI(pos = self.resmanual['circlepar'][:2],
                              size = self.resmanual['circlepar'][2]*2)
        self.p1.addItem(self.circle)
            
        self.circle.sigRegionChangeFinished.connect(self.update_resmanual)

        hist = pg.HistogramLUTItem()
        hist.axis.setPen('k')
        self.win.addItem(hist)
        hist.setImageItem(self.img)
        # For the window/circle size
        wwinsize = QDoubleSpinBox()
        wwinsize.setMinimum(0.25)
        wwinsize.setSingleStep(0.25)
        wwinsize.setValue(self.resmanual['windowsize'])
        def uwinsize():
            self.resmanual['windowsize'] = wwinsize.value()
            self.update_resmanual()

        wwinsize.valueChanged.connect(uwinsize)
        #wwinsize.valueChanged.
        layout.addRow(QLabel("Window size [mm]"),wwinsize)
        layout.setFieldGrowthPolicy(QFormLayout.FieldsStayAtSizeHint)
        
        # For the histogram equalization
        wequ = QCheckBox()
        wequ.setChecked(self.eqori)
        def ueq():
            self.eqori = not self.eqori
            wequ.setChecked(self.eqori)
            self.set_original()
        wequ.stateChanged.connect(ueq)
        layout.addRow(QLabel("Equalize histogram"),wequ)

    def set_mode_circle(self,value):
        self.circle.setVisible(value)
        if value:
            scale = 1
            pos = [0,0]
        else:
            scale = self.resmanual['resolution']
            H,W = self.oriim.shape
            pos = [-1*scale*(W-W/2),-1*scale*(H-H/2)]
        self.img.setScale(scale)
        self.img.setPos(*pos)

    def plot_allen_areas(self,fitpars,refstruct = 'VISp'):
        refregions = load_refregions()
        nrefregions = adjust_allen_areas(refregions,**fitpars)
        if not hasattr(self,'allenhandles'):
            self.allenhandles = pyqtgraph_plot_allen_areas(nrefregions,
                                                           refresh=dict(window = self.win,
                                                                        axes = self.p1))
        else:
            self.allenhandles = pyqtgraph_plot_allen_areas(nrefregions,
                                                           refresh=self.allenhandles)

    def allen_areas_visible(self,value):
        if hasattr(self,'allenhandles'):
            for p in self.allenhandles['plots']:
                p.setVisible(value)
            
    def set_original(self,oriim = None):
        if not oriim is None:
            self.oriim = oriim.squeeze()
        oriim = self.oriim.copy().astype(np.float32)
        if self.eqori:
            from ..imutils import im_adapt_hist
            oriim = oriim/np.max(np.abs(oriim))
            oriim = im_adapt_hist(oriim)
        self.img.setImage(oriim[:,::-1])
        
    def set_resmanual(self,resmanual):
        self.resmanual = resmanual
        self.circle.setPos(resmanual['circlepar'][:2])
        self.circle.setSize(resmanual['circlepar'][2]*2.)
        self.update_resmanual()
        
    def update_resmanual(self):
        pos = self.circle.pos()
        size = self.circle.size()
        self.resmanual['circlepar'] = [pos[0],pos[1],size[0]/2.]
        self.resmanual['resolution'] = self.resmanual['windowsize']/np.ceil(2*self.resmanual['circlepar'][2])
        if not self.parent is None:
            for m in self.parent.matchwidefieldccf:
                m.resolution = self.resmanual['resolution']
                m.updateImgScale()



class ManualImageOverlay(QWidget):
    def __init__(self,oriim,fitims = [], fitpars = [],names = []):
        '''
        Inits a widget with the original image and multiple fit images that 
 can be manually overlayed on the original
        
        This needs some simplification.
        '''
        super(ManualImageOverlay,self).__init__()
        self.oriim = oriim
        self.refh,self.refw = oriim.shape
        print(self.refh,self.refw)
        self.fitims = []
        self.fitpars = []
        self.names = []
        self.iref = 0

        self.init_ui()
        if len(fitims) > len(names):
            names = []
            for i,im in enumerate(fitims):
                names.append(None)
        if len(fitims) > len(fitpars):
            # resetting fitpars
            fitpars = []
            for i in fitims:
                fitpars.append(dict(refh = self.refh,
                                    refw = self.refw,
                                    rotation = 0,
                                    scale = 0.5,
                                    transpose = True,
                                    ratio = 1.,
                                    origin = None))
        for im,fitp,n in zip(fitims,fitpars,names):
            self.add_im(im,fitp,n)
 
    def init_ui(self):
        self.tab = QDockWidget('Manual fit image to reference')
        layout = QFormLayout()
        self.setLayout(layout)
        
        self.win = pg.GraphicsLayoutWidget()
        layout.addRow(self.win)
        
        self.p1 = self.win.addPlot()
        
        self.oriimview = pg.ImageItem()
        self.p1.addItem(self.oriimview)
        self.p1.getViewBox().invertY(True)
        self.p1.getAxis('bottom').setPen('k') 
        self.p1.getAxis('left').setPen('k') 

#         self.p1.getViewBox().setAspectRatio(True)

        self.fitimviews = []
        self.fitpospoints = []
        self.opacities = []
        
        pGroup = QGroupBox(self)
        pGrid = QFormLayout()
        pGroup.setLayout(pGrid)
        pGroup.setTitle("Image fit parameters")  
#         layout.addRow(pGroup)
        ''' 
             rotation = 0,
             scale = 0.5,
             transpose = True,
             ratio = 1.,
             origin = None
        '''
        # Rotation
        self.wrot = QSlider(Qt.Horizontal)
        self.wrot.setValue(0)
        self.wrot.setMaximum(90.)
        self.wrot.setMinimum(-90.)
        self.wrot.setSingleStep(0.5)
        self.wrotlabel = QLabel('Rotation {0:03.1f}:'.format(
            self.wrot.value()))
        self.wrot.valueChanged.connect(self.uRotation)
        pGrid.addRow(self.wrotlabel, self.wrot)
        # Scale
        self.wscale = QSlider(Qt.Horizontal)
        self.wscale.setValue(1000)
        self.wscale.setMaximum(1500)
        self.wscale.setMinimum(1.)
        self.wscale.setSingleStep(.5)
        self.wscalelabel = QLabel('Scale {0:01.3f}:'.format(
            self.wscale.value()/1000.))
        self.wscale.valueChanged.connect(self.uScale)
        pGrid.addRow(self.wscalelabel, self.wscale)
        # Ratio
        self.wratio = QSlider(Qt.Horizontal)
        self.wratio.setValue(100)
        self.wratio.setMaximum(200)
        self.wratio.setMinimum(1.)
        self.wratio.setSingleStep(.5)
        self.wratiolabel = QLabel('Ratio {0:01.3f}:'.format(
            self.wratio.value()/100.))
        self.wratio.valueChanged.connect(self.uRatio)
        pGrid.addRow(self.wratiolabel, self.wratio)
        # origin
        self.worigin = DraggablePoint()  # brush=pg.mkBrush(255, 255, 255, 120)))
        pos = [self.oriim.shape[1]/2.,self.oriim.shape[0]/2.]
        if len(self.fitpars) and not self.fitpars[self.iref]['origin'] is None:
            pos = [self.fitpars[self.iref]['origin'][1],
                   self.fitpars[self.iref]['origin'][0]]
        self.woriginlabel = QLabel('Origin: {0:03.1f},{1:03.1f}'.format(
            pos[0],pos[1]))        
        pos = np.array(pos).reshape([1,2])
        self.worigin.setData(pos = pos,size=10, brush=pg.mkBrush(255, 0, 0, 255))
        pGrid.addRow(self.woriginlabel)
        self.worigin.externalUpdateFunc = self.uTranslate
        self.p1.addItem(self.worigin)
        self.wtranspose = QCheckBox()
        self.wtranspose.setChecked(True)
        self.wtranspose.stateChanged.connect(self.uTranspose)
        pGrid.addRow(QLabel("Transpose"),self.wtranspose)
        # display and save
        pGroup1 = QGroupBox(self)
        pGrid = QFormLayout()
        pGroup1.setLayout(pGrid)
        pGroup1.setTitle("Display")  
        layout.addRow(pGroup1,pGroup)
        self.weqorig = QCheckBox()
        self.weqorig.setChecked(True)
        self.weqorig.stateChanged.connect(self.set_original)
        pGrid.addRow(QLabel("Equalize original"),self.weqorig)
        self.weqfit = QCheckBox()
        self.weqfit.setChecked(True)
        self.weqfit.stateChanged.connect(self.set_fitim)
        pGrid.addRow(QLabel("Equalize fit"),self.weqfit)
        self.flipx = QCheckBox()
        self.flipx.setChecked(True)
        self.flipx.stateChanged.connect(self.uflipx)
        pGrid.addRow(QLabel("Invert X"),self.flipx)
        
        self.wopac = QSlider(Qt.Horizontal)
        self.wopac.setValue(80)
        self.wopac.setMaximum(100)
        self.wopac.setMinimum(1.)
        self.wopac.setSingleStep(1)
        wopaclabel = QLabel('Opacity')
        self.wopac.valueChanged.connect(self.uOpacity)
        pGrid.addRow(wopaclabel, self.wopac)
        
        self.set_original(self.oriim)
        self.p1.getViewBox().setAspectLocked(True)
        self.p1.getViewBox().invertX(self.flipx.checkState())
        self.worigin.setZValue(10000)
        # Load and save
        pGroup = QGroupBox(self)
        pGrid = QFormLayout()
        pGroup.setLayout(pGrid)
        pGroup.setTitle("Load and Save")  
        layout.addRow(pGroup)
        self.wnames = QListWidget()
        self.wnames.itemDoubleClicked.connect(self.unames)
        pGrid.addRow(self.wnames)
        wloadImage = QPushButton('Load image')
        wloadImage.clicked.connect(self.load_image)
        wsave = QPushButton('Save')
        wsave.clicked.connect(self.save)
        wload = QPushButton('Load saved')
        wload.clicked.connect(self.load_json)

        pform = QHBoxLayout()
        pform.addWidget(wloadImage)
        pform.addWidget(wsave)
        pform.addWidget(wload)
        pGrid.addRow(pform)
        
    def load_image(self,fnames=None,fitpars = None):
        if not type(fnames) is list:
            print('Trying load')
            fnames = QFileDialog.getOpenFileNames(self,'Select files to open')[0]
            if not len(fnames):
                print('No file selected.')
                return
        ims = []
        names = []
        fpars = []
        for i,f in enumerate(fnames):
            if not os.path.isfile(f):
                print('No file:{0}'.format(f))
                continue
            im = imread(f)
            im = im.squeeze()            
            if (len(im.shape)>2):
                if not im.shape[2]==3: # then its rgb
                    im = np.mean(im,axis = 0)
                else:
                    im = np.mean(im,axis=2)
                im = im / float(np.max(im))
            ims.append(im)
            names.append(f)
            if not fitpars is None:
                fpars.append(fitpars[i])
            else:
                fpars.append(None)
        for i,(im,fitpar,f) in enumerate(zip(ims,fpars,names)):
            print('Adding: {0}'.format(f))
            self.add_im(im,fitpars = fitpar,name = f)
    def save(self):
        from copy import deepcopy
        fitpars = deepcopy(self.fitpars)
        for i,n in enumerate(self.names):
            fitpars[i]['filename'] = n
        fname = QFileDialog.getSaveFileName(self,'Choose file to save')[0]
        if not len(fname):
            print('No file selected.')
            return
        with open(fname,'w') as f:
            json.dump(fitpars,f,indent=4)
    def load_json(self):
        fname = QFileDialog.getOpenFileName(self,'Select file to open')[0]
        if not len(fname):
            print('No file selected.')
            return
        with open(fname,'r') as f:
            fitpars = json.load(f)
        names = []
        for f in fitpars:
            names.append(f['filename'])
        print(names)
        self.load_image(names,fitpars)
    def unames(self):
        txt = [item.text() for item in self.wnames.selectedItems()][0]
        self.iref = np.where(np.array(self.names) == txt)[0][0]
        self.set_fitim()
        self.update_widgets()
    def set_fitim(self):
        fitim = self.fitims[self.iref].copy()
        if self.weqfit.checkState():
            fitim = im_adapt_hist(fitim)
        self.fitim = fitim
        
    def uOpacity(self):
        self.opacities[self.iref] = self.wopac.value()/100.
        self.update()
    def uflipx(self):
        self.p1.getViewBox().invertX(self.flipx.checkState())
    def set_original(self,oriim = None):
        if not oriim is None:
            if not isinstance(oriim,int):
                self.oriim = oriim.squeeze().astype(np.float32)/oriim.max()
        oriim = self.oriim.copy().astype(np.float32)
        if self.weqorig.checkState():
            oriim = im_adapt_hist(oriim)
        self.oriimview.setImage(oriim)
    def uTranslate(self,pos):
        self.fitpars[self.iref]['origin'] = [pos[1],pos[0]]
        self.woriginlabel.setText('Origin: {0:03.1f},{1:03.1f}'.format(
            pos[0],pos[1]))
        self.update()
    def uRotation(self):
        self.fitpars[self.iref]['rotation'] = self.wrot.value()
        self.wrotlabel.setText('Rotation {0:03.1f}:'.format(
        self.wrot.value()))
        self.update()
    def uScale(self):
        self.fitpars[self.iref]['scale'] = self.wscale.value()/1000.
        self.wscalelabel.setText('Scale {0:01.3f}:'.format(
        self.wscale.value()/1000.))
        self.update()
    def uRatio(self):
        self.fitpars[self.iref]['ratio'] = self.wratio.value()/100.
        self.wratiolabel.setText('Ratio {0:01.3f}:'.format(
        self.wratio.value()/100.))
        self.update()
    def uTranspose(self):
        self.fitpars[self.iref]['transpose'] = self.wtranspose.checkState()
        self.update()

    def add_im(self,fitim,fitpars = None,name=None):
        self.fitims.append(fitim)
        if fitpars is None:
            fitpars = dict(refh = self.refh,
                           refw = self.refw,
                           rotation = 0,
                           scale = 0.2,
                           transpose = True,
                           ratio = 1.,
                           origin = None)
        self.fitpars.append(fitpars)
        if name is None:
            name  = 'referenceimage{0}'.format(len(self.names))
        self.names.append(name)
        self.iref = len(self.fitpars)-1
        self.set_fitim()
        self.opacities.append(0.5)
        self.fitimviews.append(pg.ImageItem())
        self.p1.addItem(self.fitimviews[self.iref])
        self.update_refim(self.iref)
        self.update_widgets()
        self.wnames.addItem(self.names[self.iref])
        
    def update_widgets(self):
        fpar = self.fitpars[self.iref]
        self.wrot.setValue(fpar['rotation'])
        self.wscale.setValue(fpar['scale']*1000)
        self.wratio.setValue(fpar['ratio']*100)
        self.wtranspose.setChecked(fpar['transpose'])
        pos = [self.oriim.shape[1]/2.,self.oriim.shape[0]/2.]
        if not fpar['origin'] is None:
            pos = [fpar['origin'][1],
                   fpar['origin'][0]]
        self.woriginlabel.setText('Origin: {0:03.1f},{1:03.1f}'.format(
            pos[0],pos[1]))        
        pos = np.array(pos).reshape([1,2])
        self.worigin.setData(pos = pos,size=10, brush=pg.mkBrush(255, 0, 0, 255))
        
    def update(self):
        self.update_refim(self.iref)
    def update_refim(self,iref=0):
        try:
            nim = adjust_to_reference(self.fitim,**self.fitpars[iref])
        except ValueError:
            print('adjust to reference failed.. out of bounds')
            return
        nim[np.isnan(nim)] = 0
        rgba = pg.makeRGBA(nim,levels = np.percentile(nim[nim>0],
                                                      [0,99.]))[0]
        rgba[:,:,3] = (nim > 0)*255
        self.fitimviews[iref].setImage(rgba,opacity = self.opacities[iref])
        
        

