from .utils import *

__all__ = ['MatchAllenCCF']

class MatchAllenCCF(QWidget):
    def __init__(self,mov,parent = None,resolution = 1,fitpars=None,
                 master=True,refstruct = 'VISp', name='allen Widefield Match',draw_allen=True):
        super(MatchAllenCCF,self).__init__()
        self.mov = mov
        self.refstruct = refstruct
        self.resolution = resolution
        self.autoRange = False
        self.master = master
        self.parent = parent
        self.levels = [np.min(self.mov[:]),
                       0.8*np.max(self.mov[:])]
        self.levels = np.percentile(self.mov,[5,99])
        self.name = name
        if fitpars is None:
            self.fitpars = dict(translation=np.array([0, 0]),
                                reference_structure = 'VISp',
                                rotation=0,
                                scale=1)
        else:
            self.fitpars = fitpars
        self.addMatch()
        self.timer.start(1000./30.)
        self.draw_allen = draw_allen
        self.allen_areas_visible(self.draw_allen)

    def allen_areas_visible(self,value = True):
        self.draw_allen = value
        if hasattr(self,'center_area_draggable'):
            self.center_area_draggable.setVisible(value)
        for p in self.matchhandles['plots']:
            p.setVisible(value)
        if hasattr(self,'pGroupMatch'):
            self.pGroupMatch.setVisible(value)
    def updateImgScale(self):
        scale = self.resolution
        N,H,W = self.mov.shape
        pos = [-1*scale*(W-W/2),-1*scale*(H-H/2)]
        self.img.setScale(scale)
        self.img.setPos(*pos)
            
        
    def addMatch(self):
        self.tab = QDockWidget(self.name)

        
        widget = QWidget()
        layout = QFormLayout()
        widget.setLayout(layout)
        
        window = pg.GraphicsLayoutWidget()
        layout.addRow(window)
        
        self.p1 = window.addPlot()
        self.p1.getViewBox().invertY(True)
        self.p1.getViewBox().setAspectLocked(True)
        self.p1.getAxis('bottom').setPen('k') 
        self.p1.getAxis('left').setPen('k') 

        self.img = pg.ImageItem()
        self.p1.addItem(self.img)
    
        self.Nframes = self.mov.shape[0]
        self.iFrame = 0
        self.img.setImage(self.mov[self.iFrame,:,::-1])

        self.timer = QTimer()    

        self.timer.timeout.connect(self.timer_update)

        
        self.tab.setWidget(widget)
        hist = pg.HistogramLUTItem()
        hist.axis.setPen('k')
        window.addItem(hist)
        hist.setImageItem(self.img)
        
        self.refregions = load_refregions()
        nrefregions = adjust_allen_areas(self.refregions,**self.fitpars)
        self.matchhandles = pyqtgraph_plot_allen_areas(nrefregions,
                                                       refresh=dict(window = window,
                                                                    axes = self.p1))
        if self.master:
            refstruct = np.where([x['name']==self.refstruct for x in self.refregions])[0][0]
            self.center_area_draggable = DraggablePoint()
            pos = np.array([float(self.fitpars['translation'][1]),
                            float(self.fitpars['translation'][0])]).reshape([1,2])
            self.center_area_draggable.setData(pos = pos,size=8, brush=pg.mkBrush(255, 100, 100, 200))
            def uTranslate(pos):
                self.fitpars['translation'] = [float(pos[1]),float(pos[0])]
                self.update_refregions()
            self.center_area_draggable.externalUpdateFunc = uTranslate
            #self.center_area_draggable.setData(pos = nrefregions[refstruct]['cm'].reshape((1,2)))
            
            self.p1.addItem(self.center_area_draggable)

            self.pGroupMatch = QGroupBox(self)
            pGrid = QFormLayout()
            self.pGroupMatch.setLayout(pGrid)
            self.pGroupMatch.setTitle("Match parameters")  
            # Gamma
            wrot = QSlider(Qt.Horizontal)
            wrot.setValue(self.fitpars['rotation'])
            wrot.setMaximum(90.)
            wrot.setMinimum(-90.)
            wrot.setSingleStep(.5)
            wrotlabel = QLabel('Rotation {0:3.1f}:'.format(
                wrot.value()))
            def uRotation():
                self.fitpars['rotation'] = wrot.value()
                wrotlabel.setText('Rotation {0:3.1f}:'.format(
                wrot.value()))
            wrot.valueChanged.connect(uRotation)
            wrot.valueChanged.connect(self.update_refregions)
            pGrid.addRow(wrotlabel, wrot)
            wscale = QSlider(Qt.Horizontal)

            wscale.setMaximum(150)
            wscale.setMinimum(60)
            wscale.setSingleStep(1)
            wscale.setValue(self.fitpars['scale']*100)
            wscalelabel = QLabel('Scale {0:1.3f}:'.format(
                wscale.value()/100.))
            def uScale():
                self.fitpars['scale'] = wscale.value()/100.
                wscalelabel.setText('Scale {0:1.3f}:'.format(
                wscale.value()/100.))
            wscale.valueChanged.connect(uScale)
            wscale.valueChanged.connect(self.update_refregions)
            pGrid.addRow(wscalelabel, wscale)
            layout.addRow(self.pGroupMatch)
        pGroup = QGroupBox(self)
        pGrid = QFormLayout()
        pGroup.setLayout(pGrid)
        pGroup.setTitle("")
        self.wslid = QSlider(Qt.Horizontal)
        self.wslid.setValue(0.)
        self.wslid.setMaximum(self.mov.shape[0]-1)
        self.wslid.setMinimum(0)
        self.wslid.setSingleStep(1)
        slidlabel = QLabel('Frame {0:04d}:'.format(0))
        def uslide():
            self.iFrame = self.wslid.value()
            self.timer_update()
            slidlabel.setText('Frame {0:04d}:'.format(self.iFrame))
        
        self.wslid.valueChanged.connect(uslide)
        pGrid.addRow(slidlabel,self.wslid)

        wtimer = QCheckBox()
        wtimer.setChecked(True)
        def checktimer():
            if not wtimer.checkState():
                self.timer.stop()
            else:
                self.timer.start(1/30.)
        wtimer.stateChanged.connect(checktimer)

        pGrid.addRow(QLabel('Loop:'),wtimer)
        wautorange = QCheckBox()
        wautorange.setChecked(False)
        def checkrange():
            if wautorange.checkState():
                self.autoRange = True
            else:
                self.autoRange = False
                self.levels = np.percentile(self.mov,[5,99])
        wautorange.stateChanged.connect(checkrange)
        pGrid.addRow(QLabel('Auto range:'),wautorange)

        wminframe = QSpinBox()
        wminframe.setMinimum(1)
        wminframe.setMaximum(len(self.mov)-1)
        wminframe.setSingleStep(1)
        wminframe.setValue(0)
        wmaxframe = QSpinBox()
        wmaxframe.setMinimum(1)
        wmaxframe.setMaximum(len(self.mov)-1)
        wmaxframe.setSingleStep(1)
        wmaxframe.setValue(len(self.mov)-1)

        wtimecolor = QCheckBox()
        wtimecolor.setChecked(False)
        def timecolor():
            
            if wtimecolor.checkState():
                self.timer.stop()
                self.img.clear()
                m0 = wminframe.value()
                m1 = wmaxframe.value()
                im = im_fftphase_hsv(self.mov[m0:m1,:,:])
                self.img.setImage(im[:,::-1,:],
                                  autoDownsample = False,lut=None)
                hist.hide()
            else:
                self.img.clear()
                if wtimer.checkState():
                    self.timer.start(1/30.)
                else:
                    self.timer_update()
                hist.show()

        wtimecolor.stateChanged.connect(timecolor)
        pGrid.addRow(QLabel('Color as time:'),wtimecolor)
        pGrid.addRow(wminframe,wmaxframe)


        layout.addRow(pGroup)

        
    def update_refregions(self):
        if not self.parent is None:
            for m in self.parent.matchwidefieldccf:
                m.redraw_refregions(self.fitpars)
        else:
            self.redraw_refregions(self.fitpars)
            
    def redraw_refregions(self,fitpars=None):
        if not fitpars is None:
            self.fitpars = fitpars
        nrefregions = adjust_allen_areas(self.refregions,**self.fitpars)
        self.matchhandles = pyqtgraph_plot_allen_areas(nrefregions,
                                                       refresh=self.matchhandles)


    def timer_update(self):
        self.iFrame += 1
        val = int(np.mod(self.iFrame,self.Nframes))
        nargs = {}
        if not self.autoRange:
            nargs['levels'] = self.levels
        self.img.setImage(self.mov[val,:,::-1],
                          autoDownsample = True,
                          **nargs)
        
