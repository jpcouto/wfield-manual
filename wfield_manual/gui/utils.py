from PyQt5.QtWidgets import (QApplication,
                             QWidget,
                             QDockWidget,
                             QFormLayout,
                             QHBoxLayout,
                             QMainWindow,
                             QGroupBox,
                             QSlider,
                             QWidgetAction,
                             QListWidget,
                             QComboBox,
                             QPushButton,
                             QLabel,
                             QTextEdit,
                             QDoubleSpinBox,
                             QSpinBox,
                             QCheckBox,
                             QProgressBar,
                             QFileDialog,
                             QAction)

from PyQt5 import QtCore
from PyQt5.QtCore import Qt,QTimer

import pyqtgraph as pg
pg.setConfigOption('background', [200,200,200])
pg.setConfigOption('crashWarning', True)
pg.setConfigOptions(imageAxisOrder='row-major')

import json
import os
from tifffile import imread

from ..allen import load_refregions 
from ..utils import *
from ..imutils import *
from ..vstim import *

class UpdateProgress(object):
    def __init__(self,bar,maxval = 100):
        self.bar = bar
        self.progress = 0
    def write(self, bar):
        if '%' in bar:
            percent = bar.split('%')
        else:
            percent = bar.split('it')
        try:
            self.progress = int(percent[0])
        except ValueError:
            return
        self.bar.setValue(self.progress)
        QApplication.processEvents()
    def setMaximum(self,maxval,reset  = True):
        self.bar.setMaximum(maxval)
        if reset:
            self.bar.setValue(0)
            self.progress = 0
    def setValue(self,value=0):
        self.bar.setValue(value)
        self.progress = value

class QActionCheckBox(QWidgetAction):
    def __init__(self,parent,label='',value=True):
        super(QActionCheckBox,self).__init__(parent)
        self.subw = QWidget()
        self.sublay = QFormLayout()
        self.checkbox = QCheckBox()
        self.sublab = QLabel(label)
        self.sublay.addRow(self.checkbox,self.sublab)
        self.subw.setLayout(self.sublay)
        self.setDefaultWidget(self.subw)
        self.checkbox.setChecked(value)
    def link(self,func):
        self.checkbox.stateChanged.connect(func)

class DraggablePoint(pg.GraphItem):
    def __init__(self):
        '''To drag points and update draw.'''
        self.dragPoint = None
        self.dragOffset = None
        pg.GraphItem.__init__(self)
        #self.scatter.sigClicked.connect(self.clicked)
    def externalUpdateFunc(self,pos):
        pass
    def setData(self, **kwds):
        self.data = kwds
        if 'pos' in self.data:
            npts = self.data['pos'].shape[0]
            self.data['data'] = np.empty(npts, dtype=[('index', int)])
            self.data['data']['index'] = np.arange(npts)
        self.updateGraph()
                
    def updateGraph(self):
        pg.GraphItem.setData(self, **self.data)  
        
    def mouseDragEvent(self, ev):
        if ev.button() != QtCore.Qt.LeftButton:
            ev.ignore()
            return
        if ev.isStart():
            pos = ev.buttonDownPos()
            pts = self.scatter.pointsAt(pos)
            if len(pts) == 0:
                ev.ignore()
                return
            self.dragPoint = pts[0]
            ind = pts[0].data()[0]
            self.dragOffset = self.data['pos'][ind] - pos
        elif ev.isFinish():
            self.dragPoint = None
            return
        else:
            if self.dragPoint is None:
                ev.ignore()
                return
        ind = self.dragPoint.data()[0]
        self.data['pos'][ind] = ev.pos() + self.dragOffset

        self.updateGraph()
        self.externalUpdateFunc(self.data['pos'][ind])
        ev.accept()

def pyqtgraph_plot_allen_areas(refregions,
                               reference_structure = 'VISp',
                               plotnames = False,
                               refresh = None,
                               window = None,                               
                               **kwargs):
    '''
    Plot or updates the reference structures defined by contours in reference to a reference structure.
    '''
    refpoint = [0,0]
    if refresh is None:
        refresh = dict(window = pg.GraphicsLayoutWidget())
        refresh['axes'] = refresh['window'].addPlot()
        refresh['axes'].getViewBox().invertY(True)
    update = True
    if not 'plots' in refresh.keys():
        refresh['plots'] = []
        update = False
    for i,r in enumerate(refregions):
        if update:
            refresh['plots'][i].setData(
                x = r['c'][:,1]-refpoint[1],
                y = r['c'][:,0]-refpoint[0])
        else:
            refresh['plots'].append(refresh['axes'].plot(
                np.vstack([r['c'][:,1]-refpoint[1],
                           r['c'][:,0]-refpoint[0]]).T))
        
    return refresh
