from .utils import *
from .allen import *
from .manual import *
from .raw import *
from .plugins import *

class WidefieldApp(QMainWindow):
    def __init__(self,rawfolder = None,
                 oriim = None,
                 mov=None,
                 resmanual = dict(circlepar = [222,111,291],
                                  resolution = 0.00857,
                                  windowsize = 5),
                 fitpars = None, overlays_cfg = None):
        super(WidefieldApp,self).__init__()
        '''Create tabs:
            for defining the circle; gets the resolution
            for manually matching the allen maps
        '''

        self.setWindowTitle("wfield")
        self.setDockOptions(QMainWindow.AllowTabbedDocks |
                            QMainWindow.AllowNestedDocks |
                            QMainWindow.AnimatedDocks)
        self.draw_allen = True

        self._init_menubar()
        if resmanual is None:
            resmanual = dict(circlepar = [222,111,291],
                             resolution = 0.00857,
                             windowsize = 5)
        self.resmanual = resmanual
        self.matchwidefieldccf = []
        self.fitpars = fitpars
        if not 'resolution' in self.resmanual.keys(): self.resmanual['resolution'] = 1
        if not 'windowsize' in self.resmanual.keys(): self.resmanual['windowsize'] = 5
        self.oriim = oriim
        self.circlefit = None
        self.refmatch = None
        self.rawwidget = None
        self.mov = mov
        if not rawfolder is None:
            print('Loading raw mode {0}.'.format(rawfolder))
            self.loadRaw(rawfolder)
        if not self.oriim is None:
            self.addCircleFit()
            self.circlefit.update_resmanual()
        if not mov is None:
            for m in mov:
                self.addMatch(m)
        self.show()
        
    def _init_menubar(self):
        bar = self.menuBar()
        rawmenu = bar.addMenu('Raw')
        rawmenu.addAction('Load raw tiff sequence data')
        rawmenu.triggered[QAction].connect(self.menuRaw)
        orimenu = bar.addMenu('Original')
        orimenu.addAction('Load image')
        orimenu.triggered[QAction].connect(self.menuOriginal)
        showallen = QActionCheckBox(self,'Show allen',False)
        def on_show_allen(value):
            self.circlefit.set_mode_circle(not value)
            if not self.fitpars is None:
                self.circlefit.plot_allen_areas(self.fitpars)
                self.circlefit.allen_areas_visible(value)
            return
        showallen.link(on_show_allen)
        orimenu.addAction(showallen)

        overmenu = bar.addMenu('Overlay')
        overmenu.addAction('Open')
        overmenu.triggered[QAction].connect(self.menuOverlay)
        avgmenu = bar.addMenu('Averages')
        avgmenu.addAction('Load')
        avgmenu.triggered[QAction].connect(self.menuAverages)
        avgmenu.addAction('Load allen match parameters')
        avgmenu.addAction('Plot color as time') 
        showallen = QActionCheckBox(self,'Show allen',self.draw_allen)
        def on_show_allen(value):
            self.draw_allen = value
            for m in self.matchwidefieldccf:
                m.allen_areas_visible(self.draw_allen)
            return
        showallen.link(on_show_allen)
        avgmenu.addAction(showallen)
        plotmenu = bar.addMenu('Plot')
        plotmenu.addAction('Stim as color')
        plotmenu.addAction('Time as color')
        plotmenu.triggered[QAction].connect(self.menuPlot)        

    def menuRaw(self,q):
        if q.text() == 'Load raw tiff sequence data':
            rawfolder = str(QFileDialog().getExistingDirectory(self,'Select folder to process raw data'))
            print('Selected {0}'.format(rawfolder))
            self.loadRaw(rawfolder)
    def menuOriginal(self,q):
        if q.text() == 'Load image':
            reffile = str(QFileDialog().getOpenFileName(self,'Load reference image')[0])
            print('Selected {0}'.format(reffile))
            if not reffile == '':
                reference = imread(reffile).squeeze()
                if len(reference.shape) > 2:
                    reference = reference.mean(axis = 0)
                self.oriim = reference
                self.addCircleFit()
    def menuPlot(self,q):
        if q.text() == 'Stim as color':
            PlotPlugin(self).add_to_parent()
            pass
            #self.plot_stim_as_color()
        elif q.text() == 'Time as color':
            self.plot_color_as_time()
    def menuOverlay(self,q):
        if q.text() == 'Open':
            self.addOverlay()
    def menuAverages(self,q):
        if q.text() == 'Load':
            filenames = QFileDialog().getOpenFileNames(
                self,
                'Load average movie')[0]
            if len(filenames):
                for filename in filenames:
                    m = imread(str(filename))
                    if self.circlefit is None:                    
                        self.oriim = m.mean(axis = 0)
                        self.addCircleFit()
                    self.addMatch(m)
            for m in self.matchwidefieldccf:
                m.updateImgScale()
                m.redraw_refregions(self.fitpars)
        elif q.text() == 'Load allen match parameters':
            self.load_allen_match_pars()
        elif q.text() == 'Plot color as time':
            self.plot_color_as_time()
    def loadRaw(self,folder):
        if os.path.isdir(folder):
            if self.rawwidget is None:
                self.rawwidget = RawDisplay([folder],parent = self)
                dock = QDockWidget('Raw display')
                dock.setWidget(self.rawwidget)
                self.addDockWidget(Qt.RightDockWidgetArea,dock)
            else:
                self.rawwidget.add_data(folder)

    def addOverlay(self):
        if not self.circlefit is None:
            if self.refmatch is None:
                self.refmatch = ManualImageOverlay(self.oriim)
                self.taboverlay = QDockWidget('Image overlay')
                self.addDockWidget(Qt.RightDockWidgetArea,self.taboverlay)
                self.tabifyDockWidget(self.taboverlay,self.tabmanual)
                self.taboverlay.setWidget(self.refmatch)
        else:
            print('Add an original image first.')
    def keyPressEvent(self, e):
        if e.key() == ord('S'): # save results
            pars=dict(resmanual = self.resmanual,
                      fitpars = self.matchwidefieldccf[0].fitpars)
            saveOutputFile = QFileDialog().getSaveFileName()
            if type(saveOutputFile) is tuple:
                saveOutputFile = saveOutputFile[0]
            with open(saveOutputFile,'w') as fd:
                from .utils import JsonEncoder
                json.dump(pars,fd,indent=4, sort_keys=True,cls = JsonEncoder)
            print('Saved: {0}'.format(saveOutputFile))

        elif e.key() == ord('L'): # load results
            self.load_allen_match_pars()
        elif e.key() == ord('P'): # plot results
            self.plot_color_as_time()
            
    def load_allen_match_pars(self,fname = None):
        if fname is None:
            fname = QFileDialog().getOpenFileName()
        if type(fname) is tuple:
            fname = fname[0]
        if not fname == '':
            with open(fname,'r') as fd:
                params = json.load(fd)
            self.resmanual = params['resmanual']
            self.fitpars = params['fitpars']
            if not self.circlefit is None:
                self.circlefit.set_resmanual(self.resmanual)
                self.circlefit.update_resmanual()
            if len(self.matchwidefieldccf):
                for m in self.matchwidefieldccf:
                    m.updateImgScale()
                    m.redraw_refregions(self.fitpars)
                print('Loaded: {0}'.format(fname))
        
    def plot_color_as_time(self):
        import pylab as plt
        from ..plotutils import imshow_window,plot_allen_areas
        im = self.oriim.copy()
        fitpars = self.matchwidefieldccf[0].fitpars
        #
        plt.figure(1)
        if self.circlefit.eqori:
            im = im_adapt_hist(img_as_float(im.astype(np.float32)/np.max(im)))
        extent = imshow_window(im[:,::-1],pars = self.resmanual,cmap='gray')
        plt.axis('tight')
        nrefregions = adjust_allen_areas(load_refregions(),**fitpars)
        plt.axis('off')
        plt.axis('equal')
        plot_allen_areas(nrefregions,plotnames = False,color='w')
        stds = runpar(np.std,[m.mov for m in self.matchwidefieldccf],axis=0)
        imhsvs = runpar(im_fftphase_hsv,[m.mov-np.median(m.mov) for m in self.matchwidefieldccf])
        m = np.max([np.max(m) for m in stds])*0.8
        for i,(s,p) in enumerate(zip(stds,imhsvs)):
            fig = plt.figure(i+2)
            fig.add_subplot(1,2,1)
            extent = imshow_window(s[:,::-1],pars = self.resmanual,cmap='hot',clim = [0,m])
            plt.axis('tight')
            plot_allen_areas(nrefregions,plotnames = False,color='w')
            plt.axis('off')
            plt.axis('equal')
            fig.add_subplot(1,2,2)
            plt.imshow(p[:,::-1,:],extent=extent)
            plt.axis('tight')
            plot_allen_areas(nrefregions,plotnames = False,color='w')
            plt.axis('off')
            plt.axis('equal')
        plt.show()

            
    def addMatch(self,mov):
        master = True
        stimname = 'stim {0}'.format(len(self.matchwidefieldccf))
        if len(self.matchwidefieldccf)>0:
            master = False

        self.matchwidefieldccf.append(MatchAllenCCF(mov,
                                                    resolution=self.resmanual['resolution'],
                                                    master = master,
                                                    name = stimname,
                                                    fitpars = self.fitpars,
                                                    parent = self))
        self.fitpars = self.matchwidefieldccf[0].fitpars
        self.addDockWidget(Qt.LeftDockWidgetArea,self.matchwidefieldccf[-1].tab)

        self.tabifyDockWidget(self.tabmanual,self.matchwidefieldccf[-1].tab)
        #self.matchwidefieldccf[-1].tab.setFloating(True)
            
        self.matchwidefieldccf[-1].updateImgScale()
        self.matchwidefieldccf[-1].redraw_refregions(self.fitpars)
        #self.circlefit.circle.sigRegionChangeFinished.connect(self.matchwidefieldccf[-1].updateImgScale)
    def stop_timers(self):
        for m in self.matchwidefieldccf:
            m.timer.stop()
        if not self.rawwidget is None:
            self.rawwidget.timer.stop()

    def addCircleFit(self):
        if self.circlefit is None:
            self.tabmanual = QDockWidget('Manual scale sample')
            self.addDockWidget(Qt.LeftDockWidgetArea,self.tabmanual)
            self.circlefit = ManualFitCircle(oriim=self.oriim,
                                             resmanual=self.resmanual,
                                             parent = self)
            self.tabmanual.setWidget(self.circlefit)        
        self.circlefit.set_original(self.oriim)

    def closeEvent(self, *args, **kwargs):
        pg.setConfigOption('crashWarning', False)
        self.stop_timers()
        for m in self.matchwidefieldccf:
            m.p1.close()
            m.close()
            del m
        if not self.circlefit is None:
            self.circlefit.close()
            del self.circlefit
        if not self.rawwidget is None:
            self.rawwidget.close()
            del self.rawwidget

        super(WidefieldApp, self).closeEvent(*args, **kwargs)
        
def main():
    import argparse
    import sys
    import os
    description = '''
GUI for matching 2d widefield retinotopy data to the allen common coordinate framework


Example usage:

wfield-ccf-match ~/nixsync/data/1photon/raw/190628_JC095_1P_JC/20190628_run002_00000018.tif "../../nixsync/data/analysis/190628_JC095_1P_JC/run02_retino_circling_noise_circle_whiskers/stimaverages_cam3/*.tif"

    '''
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('files',
                        metavar = 'files',
                        default = None,
                        type = str,
                        nargs = '+',
                        help = 'Input files.')
    parser.add_argument('-p','--param',
                        type = str,
                        default=None,
                        help = 'Saved results json file.')
    args = parser.parse_args()

    app = QApplication(sys.argv)
    reffile = None
    rawfolder = None
    movies = []
    params = None
    files = args.files
    if files is None:
        print('Wrong inputs.')
        return
    if files[0] == 'raw':
        if len(files) == 1:
            print('Select raw folder.')
            rawfolder = str(QFileDialog().getExistingDirectory())
            print('Selected {0}'.format(rawfolder))
        else:
            rawfolder = files[1]
    elif os.path.isfile(files[0]):
        print('Reference file: {0}'.format(files))
        reffile = files[0]
    else:
        print('Select reference filename {0}.'.format(files[0]))
        reffile = str(QFileDialog().getOpenFileName()[0])
    if len(files)>1 and rawfolder is None:
        if os.path.isfile(files[1]):
            movies = [files[1]]
        elif '*' in files[1]:
            from glob import glob
            movies = [m for m in glob(files[1])]
        if not len(movies):
            print('No movies to load {0}'.format(files[1]))
        
    pars = dict(resmanual=None,fitpars=None)
    if not args.param is None:
        if os.path.isfile(args.param):
            with open(args.param,'r') as fd:
                pars = json.load(fd)
    if not reffile is None:
        reference = imread(reffile).squeeze()
        if len(reference.shape) > 2:
            print('Averaging stack.')
            reference = reference.mean(axis = 0)
    else:
        reference = None
    if not movies is None:
        mov = [imread(m) for m in movies]
    else:
        mov = None
    wfccf = WidefieldApp(rawfolder,
                         reference,
                         mov,
                         resmanual = pars['resmanual'],
                         fitpars = pars['fitpars'])
    
    sys.exit(app.exec_())

