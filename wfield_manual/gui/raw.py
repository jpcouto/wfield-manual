from .utils import *
from ..io import *
from ..utils import *
from ..rawutils import *

__all__ = ['RawDisplay']

class RawDisplay(QWidget):
    def __init__(self,folders = [],
                 parent = None):
        super(RawDisplay,self).__init__()
        self.parent = parent
        self.folders = []
        self.seq = []
        self.blankavg = []
        self.plog = []
        self.prot = []
        self.opts = dict()
        self.stimtimes = []
        self.camtimes = []
        self.iframe = 0
        self.iexp = 0
        self.init_ui()
        [self.add_data(f) for f in folders]
    def add_data(self,folder):
        import wfield.scripts_pyvstim as vstim
        self.folders.append(folder)
        fnames = sortstr(glob(pjoin(folder,'*.tif')))
        p,f2 = os.path.split(folder)
        p,f1 = os.path.split(p)
        logfile = vstim.findVStimLog([f1,f2])
        if not logfile is None:
            log,comm = vstim.parseVStimLog(logfile)
            stimtimes,stimpars,stimopt = vstim.getStimuliTimesFromLog(logfile,log)
        else:
            print('Could not find logfile.')
            return
        print('Loading {0} files.'.format(len(fnames)))
        self.seq.append(TiffSequence(fnames))
        camtime = vstim.get_camera_time(glob(pjoin(folder,'*.camlog')),log,3,
                                        nframes = len(self.seq[-1]))
        self.plog.append(log)
        self.prot.append([stimpars,stimopt])
        self.stimtimes.append(stimtimes)
        self.camtimes.append(camtime)
        self.blankavg.append(np.mean(self.seq[-1][0:200:1,:,:],axis = 0))
        self.selexp.addItem(os.path.basename(folder))
        self.selexp.setCurrentIndex(int(len(self.seq)-1))
        #self.set_experiment()

    def set_experiment(self,i):
        self.iexp = i
        print('Selected experiment {0} - {1}'.format(self.iexp,self.folders[self.iexp]))
        self.iframe = 0
        self.set_time_slider()
        self.set_image()
    
    def init_ui(self):
        layout = QFormLayout()
        self.setLayout(layout)
        #mainWidget = QDockWidget('display')
        #layout.addRow(mainWidget)
        win = pg.GraphicsLayoutWidget()
        self.p1 = win.addPlot()
        self.p1.getViewBox().invertY(True)
        self.p1.getViewBox().setAspectLocked(True)

        self.img = pg.ImageItem()
        self.p1.addItem(self.img)
        self.p1.getAxis('bottom').setPen('k') 
        self.p1.getAxis('left').setPen('k') 

        widget = QWidget()
        slayout = QFormLayout()
        widget.setLayout(slayout)
        #dWidget = QDockWidget('controls')
        layout.addRow(widget)
        #dWidget.setWidget(widget)
        self.wtime = QSlider(Qt.Horizontal)
        self.wtimelabel = QLabel('Time {0:03.1f}:'.format(
            self.wtime.value()))
        slayout.addRow(self.wtimelabel, self.wtime)
        layout.addRow(win,widget)
        self.wsubtract = QCheckBox()
        self.wsubtract.setChecked(False)
        slayout.addRow(QLabel('Subtract'), self.wsubtract)
        def utime(val):
            i = self.wtime.value()
            t = self.camtimes[self.iexp][i]
            self.wtimelabel.setText('Time {0:03.1f}:'.format(t))
            self.set_image(i)
        self.wtime.valueChanged.connect(utime)
        self.wsubtract.stateChanged.connect(utime)

        wtimer = QCheckBox()
        wtimer.setChecked(False)
        self.timer = QTimer()
        self.timer.timeout.connect(self.timer_update)
        def checktimer():
            if not wtimer.checkState():
                self.timer.stop()
            else:
                self.timer.start(1/30.)
        wtimer.stateChanged.connect(checktimer)
        slayout.addRow(QLabel('Play'), wtimer)
        self.selexp = QComboBox()
        slayout.addRow(QLabel('Experiment'),self.selexp)
        self.selexp.currentIndexChanged.connect(self.set_experiment)

        self.opts['progress'] = QProgressBar()
        self.pbar = UpdateProgress(self.opts['progress'])
        slayout.addRow(self.opts['progress'])
        self.analysis_ui(slayout)
        self.lap_trig_ui(slayout)
    def timer_update(self):
        self.iframe += 1
        val = int(np.mod(self.iframe,len(self.seq[self.iexp])))
        t = self.camtimes[self.iexp][val]
        self.wtimelabel.setText('Time {0:03.1f}:'.format(t))
        self.set_image(val)

    def lap_trig_ui(self,layout):
        pGroup = QGroupBox(self)
        pGrid = QFormLayout()
        self.optslap = dict()
        pGroup.setTitle("Lap triggered parameters") # This should be more general in the future.  
        pGroup.setLayout(pGrid)
        # mode 'none','mean_trial','mean_post'
        self.optslap['mode'] = QComboBox()
        [self.optslap['mode'].addItem(f) for f in ['none','mean_trial','mean_post']]
        pGrid.addRow(QLabel('Mode'),self.optslap['mode'])
        # lap selection
        self.optslap['startlap'] = QSpinBox()
        self.optslap['startlap'].setMinimum(0)
        self.optslap['startlap'].setValue(0)
        pGrid.addRow(QLabel('Start lap'),self.optslap['startlap'])
        self.optslap['lastlap'] = QSpinBox()
        self.optslap['lastlap'].setMinimum(-1)
        self.optslap['lastlap'].setValue(-1)
        pGrid.addRow(QLabel('Last lap'),self.optslap['lastlap'])
        # "belt" length
        self.optslap['laplength'] = QDoubleSpinBox()
        self.optslap['laplength'].setMinimum(0)
        self.optslap['laplength'].setMaximum(10000)
        self.optslap['laplength'].setValue(150.)
        pGrid.addRow(QLabel('Lap length'),self.optslap['laplength'])
        #binsize
        self.optslap['lapbin'] = QDoubleSpinBox()
        self.optslap['lapbin'].setMinimum(0)
        self.optslap['lapbin'].setMaximum(10000)
        self.optslap['lapbin'].setValue(1.)
        pGrid.addRow(QLabel('lapbin'),self.optslap['lapbin'])
        # global baseline
        self.optslap['global_baseline'] = QCheckBox()
        self.optslap['global_baseline'].setChecked(False)
        pGrid.addRow(QLabel('Global baseline'),self.optslap['global_baseline'])

        layout.addRow(pGroup)
        def compute_lap():
            p,f2 = os.path.split(self.folders[self.iexp])
            p,f1 = os.path.split(p)
            expname = [f1,f2]
            lapidx = [self.optslap['startlap'].value(),
                      self.optslap['lastlap'].value()]
            import wfield.scripts_pyvstim as vstim
            self.pbar.setValue(0)
            lapavg,lapbaseline = vstim.get_lap_triggered(expname,
                                                         self.plog[self.iexp],
                                                         self.camtimes[self.iexp],
                                                         self.seq[self.iexp],
                                                         laplength = self.optslap['laplength'].value(),
                                                         lapbin = self.optslap['lapbin'].value(),
                                                         lapidx = lapidx,
                                                         baseline = None,
                                                         global_baseline = self.optslap['global_baseline'].checkState(),
                                                         do_df_f = self.opts['divide'].checkState(),
                                                         progress = self.pbar)

            if self.opts['dtype'].currentText() == 'uint16':
                mint = np.min(lapavg)
                maxt = np.max(lapavg)
                lapavg = lapavg - mint
                lapavg = lapavg/maxt
                lapavg = np.clip(lapavg*(2.0**16),0,2**16-1).astype('uint16')

            if not hasattr(self.parent,"tabManual"):
                self.parent.oriim = self.seq[self.iexp][0] 
                self.parent.addCircleFit()
                self.parent.circlefit.update_resmanual()
            self.parent.addMatch(lapavg)
            if self.opts['wsave'].checkState():
                print('Saving.')
                from tifffile import imsave
                try:
                    from labcams import getPreferences
                except:
                    print('Could not load labcams...')
                finally:
                    parameters = getPreferences()
                    p,f2 = os.path.split(self.folders[self.iexp])
                    p,f1 = os.path.split(p)
                    expname = [f1,f2]
                    fname = pjoin(parameters['datapaths']['dataserverpaths'][0],
                                  parameters['datapaths']['analysispaths'],
                                  expname[0],expname[1],'lapaverages_cam{0}'.format(3),
                                  'lap')
                    nfiles = len(glob(fname+'*.tif'))
                    fname = fname+'{0}.tif'.format(nfiles)
                    if not os.path.isdir(os.path.dirname(fname)):
                        os.makedirs(os.path.dirname(fname))
                    print(fname)
                    imsave(fname,lapavg.astype(self.opts['dtype'].currentText()))
        self.optslap['button'] = QPushButton('Compute lap triggered')
        self.optslap['button'].clicked.connect(compute_lap)
        pGrid.addRow(self.optslap['button'])
        
        
    def analysis_ui(self,layout):
        pGroup = QGroupBox(self)
        pGrid = QFormLayout()
        pGroup.setTitle("Triggered average parameters")  

        pGroup.setLayout(pGrid)
        #pGrid.addRow(QLabel('Loop:'),wtimer)
        # mode global,tpre,zscore
        self.opts['mode'] = QComboBox()
        [self.opts['mode'].addItem(f) for f in ['tpre','global','zscore']]
        pGrid.addRow(QLabel('Mode'),self.opts['mode'])

        # baseline median, mean, min
        self.opts['baseline_funct'] = QComboBox()
        [self.opts['baseline_funct'].addItem(f) for f in ['mean','min','median']]
        pGrid.addRow(QLabel('Global baseline function'),self.opts['baseline_funct'])
        
        #t pre number float
        self.opts['tpre'] = QDoubleSpinBox()
        self.opts['tpre'].setMinimum(0)
        self.opts['tpre'].setValue(1.0)
        pGrid.addRow(QLabel('Baseline time'),self.opts['tpre'])

        # df/f true/false
        self.opts['divide'] = QCheckBox()
        self.opts['divide'].setChecked(False)
        pGrid.addRow(QLabel('dF/F'),self.opts['divide'])

        # stimflag true/false
        self.opts['stimflag'] = QCheckBox()
        self.opts['stimflag'].setChecked(True)
        pGrid.addRow(QLabel('Add stim flag'),self.opts['stimflag'])
        
        # dtype
        self.opts['dtype'] = QComboBox()
        [self.opts['dtype'].addItem(f) for f in ['uint16','float32']]
        pGrid.addRow(QLabel('Data type'),self.opts['dtype'])

        self.opts['button'] = QPushButton('Compute average')

        self.opts['wsave'] = QCheckBox()
        self.opts['wsave'].setChecked(True)
        pGrid.addRow(QLabel('Save'),self.opts['wsave'])

        def parse_opts():
            dff = self.opts['divide'].checkState()
            txt = self.opts['baseline_funct'].currentText()
            if txt == 'min':
                bline = lambda x:np.min(x,axis = 0)
            elif txt == 'median':
                bline = lambda x:np.median(x,axis = 0)
            else:
                bline = lambda x:np.mean(x,axis = 0)
            txt = self.opts['mode'].currentText()
            if not txt == 'global':
                bline = txt
            return dict(tpre = self.opts['tpre'].value(),
                        baseline_funct = bline,
                        divide = dff,
                        add_stimflag = self.opts['stimflag'].checkState())
        def compute_average():
            self.pbar.setMaximum(len(self.stimtimes[self.iexp]))
            import wfield.scripts_pyvstim as vstim
            trigavg = vstim.triggered_average(self.seq[self.iexp],
                                        self.camtimes[self.iexp],
                                        self.stimtimes[self.iexp],
                                        progress = self.pbar,
                                        **parse_opts())
            trigavg = vstim.combine_loops(trigavg,self.prot[self.iexp][0])
            if self.opts['dtype'].currentText() == 'uint16':
                mint = np.min([np.min(t) for t in trigavg])
                maxt = np.max([np.max(t) for t in trigavg])
                trigavg = [t - mint for t in trigavg]
                trigavg = [t/maxt for t in trigavg]
                trigavg = [np.clip(t*(2.0**16),0,2**16-1).astype('uint16') for t in trigavg]

            if not hasattr(self.parent,"tabManual"):
                self.parent.oriim = self.seq[self.iexp][0] 
                self.parent.addCircleFit()
                self.parent.circlefit.update_resmanual()
            [self.parent.addMatch(m) for m in trigavg]
            if self.opts['wsave'].checkState():
                print('Saving.')
                from tifffile import imsave
                try:
                    from labcams import getPreferences
                except:
                    print('Could not load labcams...')
                finally:
                    for iStim,savg in enumerate(trigavg):
                        parameters = getPreferences()
                        p,f2 = os.path.split(self.folders[self.iexp])
                        p,f1 = os.path.split(p)
                        expname = [f1,f2]
                        fname = pjoin(parameters['datapaths']['dataserverpaths'][0],
                                      parameters['datapaths']['analysispaths'],
                                      expname[0],expname[1],'stimaverages_cam{0}'.format(3),
                                      'stim{0}.tif'.format(iStim))
                        if not os.path.isdir(os.path.dirname(fname)):
                            os.makedirs(os.path.dirname(fname))
                        print(fname)
                        imsave(fname,savg.astype(self.opts['dtype'].currentText()))
        self.opts['button'].clicked.connect(compute_average)
        pGrid.addRow(self.opts['button'])
        
        layout.addRow(pGroup)


    def set_image(self,i = None):
        if not i is None:
            self.iframe = i
        img = self.seq[self.iexp][self.iframe]
        if self.wsubtract.isChecked():
            img = img - self.blankavg[self.iexp]
        self.img.setImage(img)

    def set_time_slider(self):
        self.wtime.setValue(0)
        self.wtime.setMaximum(len(self.camtimes[self.iexp])-1)
        self.wtime.setMinimum(0)
        self.wtime.setSingleStep(1)
    
