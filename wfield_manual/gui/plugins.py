from .utils import *
from ..io import *
from ..utils import *

class PlotPlugin(QWidget):
    def __init__(self,parent):
        super(PlotPlugin,self).__init__()
        self.pname = 'Default plugin'
        self.parent = parent
        self.movs = [m.mov for m in self.parent.matchwidefieldccf]
        self.names = [m.name for m in self.parent.matchwidefieldccf]
        self.fitpars = self.parent.fitpars
        self.layout = QFormLayout()
        self.setLayout(self.layout)
        self.add_file_list()
    def add_file_list(self,sidewidget=str('Stimuli list:')):
        self.wnames = QListWidget()
        [self.wnames.addItem(n) for n in self.names]
        if type(sidewidget) is str:
            sidewidget = QLabel(sidewidget)
        self.layout.addRow(sidewidget,self.wnames)
    def add_to_parent(self):
        if not hasattr(self.parent,'plugins'):
            self.parent.plugins = []
            self.parent.plugins_docks = []
        self.parent.plugins.append(self)
        dock = QDockWidget(self.pname)
        dock.setWidget(self)
        self.parent.addDockWidget(Qt.RightDockWidgetArea,dock)
        dock.setFloating(True)

