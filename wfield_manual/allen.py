import numpy as np
import os
from os.path import join as pjoin
import json
import numpy as np

def get_reference_regions_from_allen(structures = ['RSP',
                                                   'SSs2/3',
                                                   'SSp-bfd2/3',
                                                   'SSp-tr2/3',
                                                   'SSp-ll2/3',
                                                   'SSp-ul2/3',
                                                   'VISp2/3',
                                                   'VISpm2/3',
                                                   'VISl2/3',
                                                   'VISal2/3',
                                                   'VISrl2/3',
                                                   'VISa2/3',
                                                   'VISam2/3',
                                                  ],res=0.025,
                                     gauss_filt = 0,
                                     contour_thresh = .1,
                                     forcedownload = False):
    '''
    Gets the allen reference region map.
    Highlights and finds contours of the max z projection of the selected structures.
   
    Examples:

        refregions,bmap,bvolume = get_reference_regions_from_allen(forcedownload=False)
    
        refregions,bmap,bvolume = get_reference_regions_from_allen(gauss_filt = 3,contour_thresh = 0.7)
    Inputs:
        - structures            : list of structure names
        - forcedownload = False : force downloading from the allen
        - gauss_filt = 0        : apply a gaussian filter before getting region contours
        - contour_thresh = 0.1  : threshold for region contour detection
    Returns: 
        - list of dictionaries with keys: (name,fullname,label,c (contour),cm (center of mass),props,resolution)
        - half of the max projection map
        - volume with annotated structures
    '''
    import nrrd
    from allensdk.api.queries.mouse_connectivity_api import MouseConnectivityApi
    from allensdk.config.manifest import Manifest
    from allensdk.api.queries.ontologies_api import OntologiesApi
    from allensdk.core.structure_tree import StructureTree
    from allensdk.core.reference_space import ReferenceSpace
    # the annotation download writes a file, so we will need somwhere to put it
    annotation_dir = pjoin(os.path.expanduser('~'),'.wfield')
    Manifest.safe_mkdir(annotation_dir)

    annotation_path = os.path.join(annotation_dir, 'annotation.nrrd')

    # this is a string which contains the name of the latest ccf version
    annotation_version = MouseConnectivityApi.CCF_VERSION_DEFAULT
    if not os.path.isfile(annotation_path) or forcedownload:
        mcapi = MouseConnectivityApi()
        mcapi.download_annotation_volume(annotation_version, res*1000, annotation_path)

    annotation, meta = nrrd.read(annotation_path)

    oapi = OntologiesApi()
    structure_graph = oapi.get_structures_with_sets([1])  # 1 is the id of the adult mouse structure graph

    # This removes some unused fields returned by the query
    structure_graph = StructureTree.clean_structures(structure_graph)  
    tree = StructureTree(structure_graph)    
    # build a reference space from a StructureTree and annotation volume, the third argument is 
    # the resolution of the space in microns
    rsp = ReferenceSpace(tree, annotation, [res*1000, res*1000, res*1000])
    V1 = None
    for i,struct in enumerate(structures):
        s = struct
        brain_observatory_structures = rsp.structure_tree.get_structures_by_acronym([s])
        brain_observatory_ids = [st['id'] for st in brain_observatory_structures]
        brain_observatory_mask = rsp.make_structure_mask(brain_observatory_ids)
        if V1 is None:
            V1 = brain_observatory_mask.copy()
        V1[brain_observatory_mask>0] = i+1
        #print(s,np.sum(brain_observatory_mask>0))
    bmap = V1.max(axis = 1)
    # Cut the map in half
    bmap = bmap[:,:int(bmap.shape[1]/2)]
    from skimage import measure
    refregions = []
    for i,s in enumerate(structures):
        tmp = (bmap == i+1)
        if gauss_filt > 0:
            from skimage.filters import gaussian
            tmp = gaussian(tmp.astype(np.float32),gauss_filt)
        cont = measure.find_contours(tmp, contour_thresh)[0]
        rprops = measure.regionprops(measure.label(tmp >= contour_thresh))[0]
        refregions.append(dict(name = s.strip('2/3'),
                               fullname = s,
                               label = measure.label(tmp >= contour_thresh),
                               c = cont*res,
                               cm = np.array(rprops.centroid) * res,
                               props = rprops,
                               resolution = res))
    return refregions,bmap,V1


def plot_overview_reference_regions(refregions,bmap):
    '''
    Plot the selected region contours overlayed on the max projection map.
    '''
    import pylab as plt
    plt.imshow(bmap,cmap = 'gray',
               clim = [0,1],
               extent = refregions[0]['resolution']*np.array([0,bmap.shape[1],bmap.shape[0],0]))
    for i,r in enumerate(refregions):
        plt.plot(r['c'][:,1],r['c'][:,0] ,lw = 4)
        plt.plot(r['cm'][1],r['cm'][0],'+k')
        plt.text(r['cm'][1]+0.1,
                 r['cm'][0]+0.1,r['name'],
                 va = 'top',
                 ha = 'center',
                 fontsize = 6,
                 color = 'k')

# These are utilities to skip connecting to the allen.
class RefRegionsJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        if isinstance(obj, skimage.measure._regionprops._RegionProperties):
            return 
        return json.JSONEncoder.default(self, obj)

def save_refregions(refregions,name = 'v1ref.json',refdir = None):
    if refdir is None: 
        refdir = pjoin(os.path.expanduser('~'),'.wfield')
    if not os.path.isdir(refdir):
        os.makedirs(refdir)
        print('[wfield] Creating: {0}'.format(refdir))
    import json
    fname = pjoin(refdir,name)
    # drop properties
    savekeys = ['name','fullname','c','cm','resolution']
    tosave = []
    for r in refregions:
        tosave.append({k:r[k] for k in savekeys})
    with open(fname,'w') as fid:
        json.dump(tosave,fid,cls=RefRegionsJSONEncoder)
    return fname

def load_refregions(fname = 'v1ref.json',refdir = None):
    if refdir is None: 
        refdir = pjoin(os.path.expanduser('~'),'.wfield')
    with open(pjoin(refdir,fname),'r') as fid:
        tmp = json.load(fid)
    for i,t in enumerate(tmp):
        t['c'] = np.array(t['c'])
        t['cm'] = np.array(t['cm'])
    return tmp
