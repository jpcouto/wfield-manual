from tqdm import tqdm
import numpy as np

def triggered_trials(camdata,
                     camtime,
                     stimtimes,
                     tpre = 2,
                     global_baseline = False,
                     do_df_f = True,
                     display_progress = True):
    dt = np.mean(np.diff(camtime))
    stimavgs = []
    wpre = int(int(np.ceil(tpre/dt)))
    for iStim in np.unique(stimtimes[:,0]):
        (_,iTrials,starttimes,endtimes) = stimtimes[stimtimes[:,0]==iStim,:].T
        duration =  np.max(np.round(endtimes - starttimes))
        wdur = int(np.ceil(duration/dt))
        wpost = int(wdur+wpre)
        ntrials = len(iTrials)
        stimavg = np.zeros([ntrials,wpre+wpost,
                            camdata.shape[1],
                            camdata.shape[2]],dtype=np.float32)
        for i,(iTrial,time) in tqdm(enumerate(
            zip(iTrials,starttimes))) if display_progress else enumerate(
            zip(iTrials,starttimes)):
            ii = np.where(camtime < time)[0][-1]
            F = camdata[ii-wpre:ii+wpost:1,:,:].astype(np.float32)
            if global_baseline:
                baseline = F[:].min(axis = 0)
            else:
                baseline = F[:wpre].mean(axis = 0)
            if do_df_f:
                stimavg[i] = (F - baseline)/baseline
            else:
                stimavg[i] = F - baseline
        stimavg[:,:,:10,:10] = np.min(stimavg)
        stimavg[:,wpre:wpost,:10,:10] = np.max(stimavg)
        stimavgs.append(stimavg)
    return stimavgs

def triggered_average(dat,
                      camtime,
                      stimtimes,
                      tpre = None,
                      baseline_funct = lambda x: np.mean(x,axis=0),
                      divide = False,
                      add_stimflag = True,
                      progress = None,
                      verbose = False):
    ''' 
    Compute the trial average of multiple stimuli.
    Sequenctially going over the dataset to avoid reading the same file twice.

    Inputs:
        - dat is a TiffSequence or np.array with 3 dims.
        - camtime the time of each frame in dat
        - tpre can be a list if differnet times are needed for differnt stim
        - baseline_funct is the function applied on the entire trial to subtract the mean (compute baseline). If it is 'pre' it uses the mean of the tpre time before the stim. 
        - divide is whether to do (F-Fbaseline)/Fbaseline on each trial
        - progress is progres bar like tqdm
        - add_stimflag adds a square indicating the stim at [:10,:10]

    Returns:
       - list of average matrices
    '''
    dt = np.mean(np.diff(camtime))
    stimavg = []
    stimnumber = []
    ntrials = []
    wposts = []
    wpres = []
    ustims = np.unique(stimtimes[:,0])
    if tpre is None:
        tpre = 0. 
    # To use in case different methods are needed for averaging
    if not type(tpre) is list:
        tpre = [tpre for t in ustims]
    for i,iStim in enumerate(ustims):
        (_,iTrials,starttimes,endtimes) = stimtimes[stimtimes[:,0]==iStim,:].T
        duration =  np.max(np.round(endtimes - starttimes))
        wpre = int(int(np.ceil(tpre[i]/dt)))
        wdur = int(np.ceil(duration/dt))
        wpres.append(wpre)
        wposts.append(int(wdur+wpre))
        ntrials.append(len(iTrials))
        stimnumber.append(iStim)
        stimavg.append(np.zeros([wpre+wdur+wpre,dat.shape[1],dat.shape[2]],dtype=np.float32))
        if verbose:
            print('''
Allocated array for stim {0} 
        duration {1} [{2} frames]
        pre stim {3} [{4} frames]
        {5} trials
            '''.format(iStim,duration,wdur,wpre*dt,wpre,ntrials))
    stimnumber = np.array(stimnumber)
    # iterate over trials
    from .gui.utils import UpdateProgress
    if isinstance(progress,UpdateProgress):
        progress.setMaximum(len(stimtimes))
    for i,t in tqdm(
        enumerate(stimtimes),
            file=progress) if not progress is None else tqdm(
                enumerate(stimtimes),total = len(stimtimes)):
            iStim = int(np.where(stimnumber == t[0])[0][0])
            ii = np.where(camtime < t[2])[0][-1]
            if ii+wposts[iStim]>=len(dat):
                continue
            F = dat[ii-wpres[iStim]:ii+wposts[iStim]:1,:,:].astype(np.float32)
            baseline = 0    
            if not baseline_funct is None:
                # There's probably a better way of doing this...
                if not type(baseline_funct) is type(lambda x:x):
                    if 'zscore' in baseline_funct:
                        if 'tpre' in baseline_funct:
                            std = F[:wpre].std(axis = 0)
                            F -= F[:wpre].mean(axis = 0)
                            F /= std
                        else:
                            std = F.std(axis = 0)
                            F -= F.mean(axis = 0)
                            F /= std
                    elif 'tpre' in baseline_funct:
                        baseline = F[:wpre].mean(axis = 0)
                else:
                    baseline = baseline_funct(F)
            if divide:
                stimavg[iStim] += ((F - baseline)/baseline)/np.float32(ntrials[iStim])
            else:
                stimavg[iStim] += (F - baseline)/np.float32(ntrials[iStim])
        # adds the stimulus time flag
    for iStim in range(len(stimavg)):
        if add_stimflag:
            stimavg[iStim][:,:10,:10] = np.min(stimavg[iStim])
            stimavg[iStim][wpres[iStim]:wposts[iStim],:10,:10] = np.max(stimavg[iStim])
    return stimavg

def bin_frames_to_laps(laps,
                       time,
                       position,
                       frames,
                       lapspace = None,
                       velocity = None,
                       velocityThreshold = 1.,
                       baseline = None,
                       global_baseline = False,
                       do_df_f = True,
                       baseline_method=lambda x : np.nanmean(x,axis=0),
                       method=lambda x : np.nanmean(x,axis=0),
                       progress = None):
    '''
    Bins a set of frames to lap position.
        - laps is [numberOfLaps,2] the start and stop time of each lap
    
    '''
    if lapspace is None:
        lapspace = np.arange(0,1.,1.)
    lapX = np.zeros((lapspace.shape[0]-1,frames.shape[1],frames.shape[2],),
                    dtype=np.float32)
    s,e = (laps[0,0],laps[-1,1])
    if not velocity is None:
        idx = np.where((time>=s) & (time<e) &
                       (velocity > velocityThreshold))[0].astype(int)
    else:
        idx = np.where((time>=s) & (time<e))[0].astype(int)

    x = frames[idx,:,:]
    pos = position[idx]
    inds = np.digitize(pos, lapspace)
    if not global_baseline or baseline is None:
        print('Computing baseline with {0} frames.'.format(len(x)))
        baseline = baseline_method(x)
    ubins = np.unique(inds)[:-1]
    from .gui.utils import UpdateProgress
    if isinstance(progress,UpdateProgress):
        progress.setMaximum(100)
    for i in tqdm(ubins,
                  file=progress) if not progress is None else tqdm(
                        ubins,total = len(ubins)):
        F = x[(inds==i),:,:].astype(np.float32)
        if do_df_f:
            dF = (F -  baseline)/baseline
        else:
            dF = F - baseline
        lapX[i-1,:,:] += method(dF)
    return lapX

