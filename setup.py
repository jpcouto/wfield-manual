#!/usr/bin/env python
# Install script for widefield utilities
# Joao Couto - Feb 2019

from setuptools import setup
from setuptools.command.install import install


longdescription = '''Utilities to look at widefield data and align with the allen reference map.'''
setup(
    name = 'wfield_manual',
    version = '0.0',
    author = 'Joao Couto',
    author_email = 'jpcouto@gmail.com',
    description = (longdescription),
    long_description = longdescription,
    license = 'GPL',
    packages = ['wfield_manual'],
    entry_points = {
        'console_scripts': [
            'wfield_manual = wfield_manual.gui.gui:main',
        ]
    },
)
